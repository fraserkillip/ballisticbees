package com.fraserkillip.ballisticbees.player;

public enum PlayerDirective {
	NIL, LEFT, RIGHT, FIRE, PAUSE
}