package com.fraserkillip.ballisticbees.player;

import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.gui.AbstractComponent;
import org.newdawn.slick.gui.GUIContext;

import com.fraserkillip.ballisticbees.BallisticBees;
import com.fraserkillip.ballisticbees.GameInfo;
import com.fraserkillip.ballisticbees.common.ITickable;
import com.fraserkillip.ballisticbees.entity.ball.EntityBall;
import com.fraserkillip.ballisticbees.entity.brick.EntityBrick;
import com.fraserkillip.ballisticbees.entity.paddle.EntityPaddle;
import com.fraserkillip.ballisticbees.game.Game;
import com.fraserkillip.ballisticbees.game.Game.GameState;
import com.fraserkillip.ballisticbees.graphics.helper.GraphicsHelper;

public class PlayerController extends AbstractComponent implements ITickable {

	private Player playerType;

	private Game game;

	private EntityPaddle paddle;

	public EntityPaddle getPaddle() {
		return paddle;
	}

	public void setPaddle(EntityPaddle paddle) {
		this.paddle = paddle;
	}

	private PlayerState state = PlayerState.Waiting;

	private EntityBall loadedBall;

	private PlayerDirective directive = PlayerDirective.NIL;

	private boolean leftKeyDown = false, rightKeyDown = false,
			fireKeyDown = false;

	private float AITime = 0;

	// Debug vars
	private Line paddleTopPlane = null;
	private Line brickToIntersection = null;
	private Line bestPaddleLine = null;
	private Vector2f intersectionPoint = null;
	private Vector2f idealTangent = null;
	private Vector2f tangent = null;
	private float[] chosenPoint = null;
	private Vector2f chosenLineVector = null;

	public PlayerController(GUIContext container, Player playerType, Game game, EntityPaddle paddle) {
		super(container);

		this.playerType = playerType;

		this.game = game;

		this.paddle = paddle;
	}

	@Override
	public void tick(float delta) {
		if (state == PlayerState.Waiting) {
			float xPos = paddle.getBoundingBox().getCenterX() - loadedBall.getBoundingBox().getWidth() / 2;
			float yPos = paddle.getBoundingBox().getMinY() - loadedBall.getBoundingBox().getHeight() - 2;
			loadedBall.setPosition(new Vector2f(xPos, yPos));

			if (directive == PlayerDirective.FIRE && loadedBall != null) {
				loadedBall.setVelocity(new Vector2f(0, -420));
				loadedBall.getVelocity().add(Game.getRandom().nextInt(50) - 25);
				loadedBall = null;
				state = PlayerState.Active;
			}
		} else {
			if (AITime > 0) {
				AITime -= delta;
			}

			if (playerType == Player.AI || BallisticBees.autoPlay || AITime > 0) {
				directive = AI();
			}
		}
	}

	@Override
	public void keyPressed(int key, char c) {
		super.keyPressed(key, c);

		int matchkey = 0;
		if (playerType == Player.Player1) {
			matchkey = BallisticBees.getSettings().Player1_fire;
		} else {
			matchkey = BallisticBees.getSettings().Player2_fire;
		}

		if (key == matchkey) {
			fireKeyDown = true;
			directive = PlayerDirective.FIRE;
			return;
		}

		if (playerType == Player.Player1) {
			matchkey = BallisticBees.getSettings().Player1_left;
		} else {
			matchkey = BallisticBees.getSettings().Player2_left;
		}

		if (key == matchkey) {
			leftKeyDown = true;
			directive = PlayerDirective.LEFT;
			return;
		}

		if (playerType == Player.Player1) {
			matchkey = BallisticBees.getSettings().Player1_right;
		} else {
			matchkey = BallisticBees.getSettings().Player2_right;
		}

		if (key == matchkey) {
			rightKeyDown = true;
			directive = PlayerDirective.RIGHT;
			return;
		}
	}

	@Override
	public void keyReleased(int key, char c) {
		super.keyReleased(key, c);

		int matchkey = 0;
		if (playerType == Player.Player1) {
			matchkey = BallisticBees.getSettings().Player1_fire;
		} else {
			matchkey = BallisticBees.getSettings().Player2_fire;
		}

		if (key == matchkey) {
			fireKeyDown = false;
		}

		if (playerType == Player.Player1) {
			matchkey = BallisticBees.getSettings().Player1_left;
		} else {
			matchkey = BallisticBees.getSettings().Player2_left;
		}

		if (key == matchkey) {
			leftKeyDown = false;
		}

		if (playerType == Player.Player1) {
			matchkey = BallisticBees.getSettings().Player1_right;
		} else {
			matchkey = BallisticBees.getSettings().Player2_right;
		}

		if (key == matchkey) {
			rightKeyDown = false;
		}

		if (fireKeyDown) {
			directive = PlayerDirective.FIRE;
		} else if (leftKeyDown) {
			directive = PlayerDirective.LEFT;
		} else if (rightKeyDown) {
			directive = PlayerDirective.RIGHT;
		} else {
			directive = PlayerDirective.NIL;
		}

	}

	public PlayerDirective getDirective() {
		return directive;
	}

	public PlayerDirective AI() {
		if (state == PlayerState.Waiting) { return PlayerDirective.FIRE; }

		if (game.currentLevel.getDisplayedRows().size() > 0 && game.currentState == GameState.PLAYING) {
			// Get the lowest ball
			EntityBall lowestBall = null;
			for (EntityBall ball : game.balls) {
				if (lowestBall == null) {
					lowestBall = ball;
					continue;
				}

				if (ball.getPosition().getY() > lowestBall.getPosition().getY()) {
					lowestBall = ball;
				}
			}

			paddleTopPlane = new Line(0, paddle.getBoundingBox().getMinY(), GameInfo.GAME_WIDTH, paddle.getBoundingBox().getMinY());
			intersectionPoint = paddleTopPlane.intersect(new Line(new Vector2f(lowestBall.getBoundingBox().getCenter()), lowestBall.getVelocity().copy().scale(10)), true);

			if (intersectionPoint != null) {
				EntityBrick closestBrick = null;
				for (List<EntityBrick> row : game.currentLevel.getDisplayedRows()) {
					for (EntityBrick brick : row) {
						if (brick.isDead()) continue;

						if (closestBrick == null) {
							closestBrick = brick;
							continue;
						}

						if (brick.getPosition().distanceSquared(intersectionPoint) < closestBrick.getPosition().distanceSquared(intersectionPoint)) {
							closestBrick = brick;
						}
					}
				}

				brickToIntersection = new Line(intersectionPoint, new Vector2f(closestBrick.getBoundingBox().getCenter()));

				tangent = new Vector2f(brickToIntersection.getNormal(0));

				if (tangent.y > intersectionPoint.y) {
					tangent.negateLocal();
				}

				idealTangent = tangent.copy();
				idealTangent = idealTangent.add(idealTangent.getTheta() + lowestBall.getVelocity().negate().getTheta()).getPerpendicular().normalise();

				double idealAngle = idealTangent.getTheta();

				bestPaddleLine = null;
				chosenLineVector = null;

				float[] points = paddle.getBoundingBox().getPoints();
				for (int i = 0; i < points.length - 3; i += 2) {
					Line line = new Line(points[i], points[i + 1], points[i + 2], points[i + 3]);
					Vector2f lineVector = new Vector2f(points[i] - points[i + 2], points[i + 1] - points[i + 3]).getNormal().normalise().negate();
					if (bestPaddleLine == null) {
						bestPaddleLine = line;
					}

					if (chosenLineVector == null) {
						chosenLineVector = lineVector;
						chosenPoint = new float[] { points[i], points[i + 1] };
					}
					if (Math.abs(lineVector.getTheta() - idealAngle) < Math.abs(chosenLineVector.getTheta() - idealAngle)) {
						bestPaddleLine = line;
						chosenLineVector = lineVector;
						chosenPoint = new float[] { points[i], points[i + 1] };
					}
				}

			} else {
				brickToIntersection = null;
				idealTangent = null;
				tangent = null;
				bestPaddleLine = null;
				chosenLineVector = null;
				chosenPoint = null;
			}

			// Set the paddle position
			if (lowestBall.getBoundingBox().getMaxY() < paddle.getBoundingBox().getMinY()) {
				float newX = 400;
				if (chosenPoint == null) {
					newX = lowestBall.getBoundingBox().getCenterX() - paddle.getBoundingBox().getWidth() / 2;
				} else {
					newX = paddle.getPosition().x + (intersectionPoint.x - chosenPoint[0]);
				}

				newX = Math.min(newX, GameInfo.GAME_WIDTH - paddle.getBoundingBox().getWidth());
				newX = Math.max(newX, 0);

				if (paddle.getPosition().x > newX) {
					return PlayerDirective.LEFT;
				} else {
					return PlayerDirective.RIGHT;
				}
			}
		}
		return PlayerDirective.NIL;
	}

	public void drawDebug(Graphics g) {
		GraphicsHelper.pushState(g);

		g.setColor(Color.red);
		if (intersectionPoint != null) {
			g.drawOval(intersectionPoint.x, intersectionPoint.y, 3, 3);
		}

		if (brickToIntersection != null) {
			g.drawGradientLine(brickToIntersection.getX1(), brickToIntersection.getY1(), Color.red, brickToIntersection.getX2(), brickToIntersection.getY2(), Color.green);
		}

		if (tangent != null) {
			tangent = tangent.getPerpendicular();
			g.drawGradientLine(intersectionPoint.x, intersectionPoint.y, Color.red, intersectionPoint.x + tangent.x * 20, intersectionPoint.y + tangent.y * 20, Color.green);
		}

		g.setColor(Color.red);
		if (bestPaddleLine != null) {
			g.draw(bestPaddleLine);
		}

		if (chosenPoint != null) {
			g.drawOval(chosenPoint[0], chosenPoint[1], 3, 3);
		}

		GraphicsHelper.popState(g);
	}

	@Override
	public int getHeight() {
		return 0;
	}

	@Override
	public int getWidth() {
		return 0;
	}

	@Override
	public int getX() {
		return 0;
	}

	@Override
	public int getY() {
		return 0;
	}

	@Override
	public void render(GUIContext container, Graphics g) throws SlickException {
	}

	@Override
	public void setLocation(int x, int y) {
	}

	public PlayerState getState() {
		return state;
	}

	public void setState(PlayerState state) {
		this.state = state;
	}

	public EntityBall getLoadedBall() {
		return loadedBall;
	}

	public void setLoadedBall(EntityBall loadedBall) {
		this.loadedBall = loadedBall;
	}

	public void addAITime(float timeRemaining) {
		AITime += timeRemaining;
	}

}
