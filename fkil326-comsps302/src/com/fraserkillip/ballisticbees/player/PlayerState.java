package com.fraserkillip.ballisticbees.player;

public enum PlayerState {
	Active, Waiting
}
