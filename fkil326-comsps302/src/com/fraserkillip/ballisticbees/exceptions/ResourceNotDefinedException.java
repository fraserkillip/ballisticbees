package com.fraserkillip.ballisticbees.exceptions;

@SuppressWarnings("serial")
public class ResourceNotDefinedException extends BallisticBeesException {
	
	private String name;
	
	public ResourceNotDefinedException(String name) {
		this.name = name;
	}
	
	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return "The requested resource: " + name + " could not be found";
	}

}
