package com.fraserkillip.ballisticbees.exceptions;

@SuppressWarnings("serial")
public class ObjectNotCollidableException extends BallisticBeesException {

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return "Entity must implement ICollidable to be added to a collision layer.";
	}

}
