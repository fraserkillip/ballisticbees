package com.fraserkillip.ballisticbees.exceptions;

@SuppressWarnings("serial")
public class AlreadyInWorldException extends BallisticBeesException {

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return "An entity cannot be added to a world twice.";
	}

}
