package com.fraserkillip.ballisticbees.exceptions;

@SuppressWarnings("serial")
public class PackNotLoadedException extends BallisticBeesException {
	private String dep, caller;

	public PackNotLoadedException(String dep, String caller) {
		this.dep = dep;
		this.caller = caller;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return "The pack: " + dep + " is required to be loaded by: " + caller;
	}

}
