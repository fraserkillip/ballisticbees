package com.fraserkillip.ballisticbees.exceptions;

@SuppressWarnings("serial")
public class ResourceAlreadyDefinedException extends BallisticBeesException {
	private String name;

	public ResourceAlreadyDefinedException(String name) {
		this.name = name;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return "The requested resource: " + name + " has already been defined.";
	}
}
