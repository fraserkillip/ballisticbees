package com.fraserkillip.ballisticbees.exceptions;

@SuppressWarnings("serial")
public class IllegalEventListenerException extends Exception {
	@Override
	public String getMessage() {
		return "Method does not have the signature public EventPropagation name(<? extends Event>)";
	}
}
