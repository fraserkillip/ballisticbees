package com.fraserkillip.ballisticbees.pack.registry;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Image;

import com.fraserkillip.ballisticbees.pack.resources.ImageResource;
import com.fraserkillip.ballisticbees.pack.resources.Resource;

public class ImageRegistry extends Registry<Image> {

	public ImageRegistry() {}
	
	public void registerImage(String name, String file) {
		registerResource(new ImageResource(name, file));
	}
	
	public Image getImage(String name) {
		return getResource(name).getInstance();
	}
	
	public List<Image> getImages(String[] names) {
		List<Image> images = new ArrayList<Image>();
		for(Resource<? extends Image> res : getResources(names)) {
			images.add(res.getInstance());
		}
		return images;
	}

}
