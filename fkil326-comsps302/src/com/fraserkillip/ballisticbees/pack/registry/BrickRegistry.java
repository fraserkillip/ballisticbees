package com.fraserkillip.ballisticbees.pack.registry;

import com.fraserkillip.ballisticbees.entity.brick.EntityBrick;
import com.fraserkillip.ballisticbees.pack.resources.BrickResource;

public class BrickRegistry extends Registry<EntityBrick> {

	public BrickRegistry() {}

	public void registerBrick(String name, int width, int height, int textureOffsetX, int textureOffsetY, String[] images, String sound) {
		registerResource(new BrickResource(name, width, height, textureOffsetX, textureOffsetY, images, sound));
	}
	
	public EntityBrick getBrick(String name) {
		return getResource(name).getInstance();
	}
}
