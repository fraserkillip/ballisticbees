package com.fraserkillip.ballisticbees.pack.registry;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.fraserkillip.ballisticbees.exceptions.BallisticBeesException;
import com.fraserkillip.ballisticbees.exceptions.ResourceAlreadyDefinedException;
import com.fraserkillip.ballisticbees.exceptions.ResourceNotDefinedException;
import com.fraserkillip.ballisticbees.pack.resources.Resource;

public abstract class Registry<T> {
	private HashMap<String, Resource<? extends T>> resources = new HashMap<String, Resource<? extends T>>();

	protected void registerResource(Resource<T> res) throws BallisticBeesException {
		if(resources.containsKey(res.getUid())) {
			throw new ResourceAlreadyDefinedException(res.getUid());
		}
		resources.put(res.getUid(), res);
	}

	protected Resource<? extends T> getResource(String uid) {
		if (resources.containsKey(uid)) {
			return resources.get(uid);
		} else {
			throw new ResourceNotDefinedException(uid);
		}
	}

	protected List<Resource<? extends T>> getResources(String[] uids) {
		List<Resource<? extends T>> list = new ArrayList<Resource<? extends T>>();

		for (String uid : uids) {
			if (resources.containsKey(uid)) {
				list.add(resources.get(uid));
			} else {
				throw new ResourceNotDefinedException(uid);
			}
		}

		return list;
	}
	
	protected Collection<Resource<? extends T>> getAllResources() {
		return resources.values();
	}
}
