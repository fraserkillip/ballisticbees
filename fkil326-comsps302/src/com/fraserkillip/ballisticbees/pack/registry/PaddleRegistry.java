package com.fraserkillip.ballisticbees.pack.registry;

import com.fraserkillip.ballisticbees.entity.paddle.EntityPaddle;
import com.fraserkillip.ballisticbees.exceptions.BallisticBeesException;
import com.fraserkillip.ballisticbees.pack.resources.PaddleResource;

public class PaddleRegistry extends Registry<EntityPaddle> {
	
	public PaddleRegistry() {}
	
	public void registerPaddle(String name, int width, int height, int textureOffsetX, int textureOffsetY, String image, String sound) throws BallisticBeesException {
		registerResource(new PaddleResource(name, width, height, textureOffsetX, textureOffsetY, image, sound));
	}
	
	public EntityPaddle getPaddle(String name) throws BallisticBeesException {
		return getResource(name).getInstance();
	}
}
