package com.fraserkillip.ballisticbees.pack.registry;

import com.fraserkillip.ballisticbees.exceptions.BallisticBeesException;
import com.fraserkillip.ballisticbees.level.Level;
import com.fraserkillip.ballisticbees.pack.definitions.LevelDefinition;
import com.fraserkillip.ballisticbees.pack.resources.LevelResource;

public class LevelRegistry extends Registry<Level> {
	
	public void registerLevel(String name, LevelDefinition levelDef) throws BallisticBeesException {
		registerResource(new LevelResource(name, levelDef));
	}
	
	public Level getLevel(String name) throws BallisticBeesException {
		return getResource(name).getInstance();
	}

}
