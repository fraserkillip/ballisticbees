package com.fraserkillip.ballisticbees.pack.registry;

import com.fraserkillip.ballisticbees.entity.ball.EntityBall;
import com.fraserkillip.ballisticbees.exceptions.BallisticBeesException;
import com.fraserkillip.ballisticbees.pack.resources.BallResource;

public class BallRegistry extends Registry<EntityBall> {
	
	public BallRegistry() {}
	
	public void registerBall(String name, int radius, float speed, int textureOffsetX, int textureOffsetY, String image) throws BallisticBeesException {
		registerResource(new BallResource(name, radius, speed, textureOffsetX, textureOffsetY, image));
	}
	
	public EntityBall getBall(String name) throws BallisticBeesException {
		return getResource(name).getInstance();
	}
}
