package com.fraserkillip.ballisticbees.pack.registry;

import java.util.ArrayList;
import java.util.List;

import com.fraserkillip.ballisticbees.exceptions.BallisticBeesException;
import com.fraserkillip.ballisticbees.pack.definitions.GameDefinition;
import com.fraserkillip.ballisticbees.pack.resources.GameResource;
import com.fraserkillip.ballisticbees.pack.resources.Resource;

public class GameRegistry extends Registry<GameDefinition> {

	public void registerGame(String name, GameDefinition gameDef) throws BallisticBeesException {
		registerResource(new GameResource(name, gameDef));
	}

	public GameDefinition getGame(String name) throws BallisticBeesException {
		return getResource(name).getInstance();
	}

	public List<GameDefinition> getAllGames() {
		List<GameDefinition> games = new ArrayList<GameDefinition>();

		for (Resource<? extends GameDefinition> res : getAllResources()) {
			games.add(res.getInstance());
		}

		return games;
	}

}
