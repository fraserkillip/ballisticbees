package com.fraserkillip.ballisticbees.pack.registry;

import java.util.HashMap;

import com.fraserkillip.ballisticbees.BallisticBees;
import com.fraserkillip.ballisticbees.exceptions.BallisticBeesException;
import com.fraserkillip.ballisticbees.exceptions.PackNotLoadedException;
import com.fraserkillip.ballisticbees.pack.Pack;
import com.fraserkillip.ballisticbees.pack.definitions.BallDefinition;
import com.fraserkillip.ballisticbees.pack.definitions.BrickDefinition;
import com.fraserkillip.ballisticbees.pack.definitions.GameDefinition;
import com.fraserkillip.ballisticbees.pack.definitions.ImageDefinition;
import com.fraserkillip.ballisticbees.pack.definitions.LevelDefinition;
import com.fraserkillip.ballisticbees.pack.definitions.PaddleDefinition;
import com.fraserkillip.ballisticbees.pack.definitions.SoundDefinition;

public class PackRegistry {

	private HashMap<String, Pack> packs = new HashMap<String, Pack>();

	public void registerPack(Pack pack) {
		if (packs.containsKey(pack.getId())) {
			System.err.println("Attemted to register pack: " + pack.getId() + "multiple times.");
			return;
		}

		packs.put(pack.getId(), pack);
	}

	private void resolveDependencies() throws PackNotLoadedException {
		for (Pack pack : packs.values()) {
			for (String dependency : pack.getDependencies()) {
				if (!packs.containsKey(dependency)) { throw new PackNotLoadedException(dependency, pack.getId()); }
			}
		}
	}

	public void initPacks() throws BallisticBeesException {
		System.out.println("Checking for missing dependencies...");
		resolveDependencies();
		System.out.println(" done!");

		for (Pack pack : packs.values()) {
			// TODO: Load images
			System.out.println("Registering images...");
			for (ImageDefinition image : pack.getImages()) {
				System.out.println("\tRegistering image: " + pack.getId() + ":" + image.getId());
				BallisticBees.getImageRegistry().registerImage(pack.getId() + ":" + image.getId(), pack.getImageUrl() + image.getFilename());
			}

			// TODO: Load sounds
			System.out.println("Registering sounds...");
			for(SoundDefinition sound : pack.getSounds()) {
				System.out.println("\tRegistering sound: " + pack.getId() + ":" + sound.getId());
				BallisticBees.getSoundRegistry().registerImage(pack.getId() + ":" + sound.getId(), pack.getSoundUrl() + sound.getFilename());
			}

			// TODO: Load Bricks
			System.out.println("Registering bricks...");
			for(BrickDefinition brick : pack.getBricks()) {
				System.out.println("\tRegistering brick: " + pack.getId() + ":" + brick.getId());
				BallisticBees.getBrickRegistry().registerBrick(pack.getId() + ":" + brick.getId(), brick.getWidth(), brick.getHeight(), brick.getTextureOffsetX(), brick.getTextureOffsetY(), brick.getImages(), brick.getBreakSound());
			}

			// TODO: Load Balls
			System.out.println("Registering balls...");
			for(BallDefinition ball : pack.getBalls()) {
				System.out.println("\tRegistering ball: " + pack.getId() + ":" + ball.getId());
				BallisticBees.getBallRegistry().registerBall(pack.getId() + ":" + ball.getId(), ball.getRadius(), ball.getSpeed(), ball.getTextureOffsetX(), ball.getTextureOffsetY(), ball.getImage());
			}

			// TODO: Load powerups

			// TODO: Load Paddles
			System.out.println("Registering paddles...");
			for(PaddleDefinition paddle : pack.getPaddles()) {
				System.out.println("\tRegistering paddle: " + pack.getId() + ":" + paddle.getId());
				BallisticBees.getPaddleRegistry().registerPaddle(pack.getId() + ":" + paddle.getId(), paddle.getWidth(), paddle.getHeight(), paddle.getTextureOffsetX(), paddle.getTextureOffsetY(), paddle.getImage(), paddle.getBounceSound());
			}
			// TODO: Load Levels
			System.out.println("Registering Levels...");
			for(LevelDefinition level : pack.getLevels()) {
				System.out.println("\tRegistering level: " + pack.getId() + ":" + level.getId());
				BallisticBees.getLevelRegistry().registerLevel(pack.getId() + ":" + level.getId(), level);
			}

			// TODO: Load games
			System.out.println("Registering games...");
			for(GameDefinition game : pack.getGames()) {
				System.out.println("\tRegistering game: " + pack.getId() + ":" + game.getId());
				BallisticBees.getGameRegistry().registerGame(pack.getId() + ":" + game.getId(), game);
			}
		}
	}
}
