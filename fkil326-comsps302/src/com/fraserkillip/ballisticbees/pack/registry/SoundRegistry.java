package com.fraserkillip.ballisticbees.pack.registry;

import org.newdawn.slick.Sound;

import com.fraserkillip.ballisticbees.pack.resources.SoundResource;

public class SoundRegistry extends Registry<Sound> {

	public SoundRegistry() {
	}

	public void registerImage(String name, String file) {
		registerResource(new SoundResource(name, file));
	}

	public Sound getSound(String name) {
		return getResource(name).getInstance();
	}
}
