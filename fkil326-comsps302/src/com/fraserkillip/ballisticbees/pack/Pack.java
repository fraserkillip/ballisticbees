package com.fraserkillip.ballisticbees.pack;

import com.fraserkillip.ballisticbees.pack.definitions.BallDefinition;
import com.fraserkillip.ballisticbees.pack.definitions.BrickDefinition;
import com.fraserkillip.ballisticbees.pack.definitions.GameDefinition;
import com.fraserkillip.ballisticbees.pack.definitions.ImageDefinition;
import com.fraserkillip.ballisticbees.pack.definitions.LevelDefinition;
import com.fraserkillip.ballisticbees.pack.definitions.LevelFileDefinition;
import com.fraserkillip.ballisticbees.pack.definitions.PaddleDefinition;
import com.fraserkillip.ballisticbees.pack.definitions.SoundDefinition;

public class Pack {

	private String id;

	private String name;

	private String[] dependencies;
	
	private BallDefinition[] balls;

	private BrickDefinition[] bricks;

	private ImageDefinition[] images;
	
	private SoundDefinition[] sounds;
	
	private GameDefinition[] games;
	
	private PaddleDefinition[] paddles;

	private LevelFileDefinition[] levelFiles;
	
	private LevelDefinition[] levels;
	
	private String imageUrl;
	
	private String soundUrl;

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String[] getDependencies() {
		return dependencies;
	}

	public BallDefinition[] getBalls() {
		return balls;
	}

	public BrickDefinition[] getBricks() {
		return bricks;
	}

	public ImageDefinition[] getImages() {
		return images;
	}

	public SoundDefinition[] getSounds() {
		return sounds;
	}
	
	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getSoundUrl() {
		return soundUrl;
	}

	public void setSoundUrl(String soundUrl) {
		this.soundUrl = soundUrl;
	}

	public GameDefinition[] getGames() {
		return games;
	}

	public LevelFileDefinition[] getLevelFiles() {
		return levelFiles;
	}
	
	public LevelDefinition[] getLevels() {
		return levels;
	}
	
	public void setLevels(LevelDefinition[] levels) {
		this.levels = levels;
	}

	public PaddleDefinition[] getPaddles() {
		return paddles;
	}
}
