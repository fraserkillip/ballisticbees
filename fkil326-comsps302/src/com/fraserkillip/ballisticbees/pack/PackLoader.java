package com.fraserkillip.ballisticbees.pack;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.fraserkillip.ballisticbees.BallisticBees;
import com.fraserkillip.ballisticbees.pack.definitions.LevelDefinition;
import com.fraserkillip.ballisticbees.pack.definitions.LevelFileDefinition;
import com.google.gson.Gson;

public class PackLoader {
	public static void loadPacks() {
		File packsDir = new File("packs/");
		
		int packCount = 0;
		
		for(File packDir : packsDir.listFiles()) {
			
			if(packDir.getName().startsWith(".")) continue;
			
			if(!packDir.isDirectory()) continue;
			
			System.out.println("Loading pack from: packs/" + packDir.getName() + "/");			
			File packFile = new File(packDir, packDir.getName() + ".json");
			
			FileReader reader;
			try {
				reader = new FileReader(packFile);
				Pack pack = new Gson().fromJson(reader, Pack.class);
				pack.setImageUrl(packDir.getCanonicalPath() + "/assets/images/");
				pack.setSoundUrl(packDir.getCanonicalPath() + "/assets/sounds/");
				LevelDefinition[] levels = new LevelDefinition[pack.getLevelFiles().length];
				int i = 0;
				System.out.println("\tLoading Levels..");
				for(LevelFileDefinition levelFileDef : pack.getLevelFiles()) {
					System.out.println("\t\t Loading " + levelFileDef.getId() + " from assets/levels/" + levelFileDef.getFilename());
					File levelFile = new File(packDir, "assets/levels/" + levelFileDef.getFilename());
					reader = new FileReader(levelFile);
					LevelDefinition level = new Gson().fromJson(reader, LevelDefinition.class);
					level.setId(levelFileDef.getId());
					levels[i++] = level;
				}
				pack.setLevels(levels);				
				BallisticBees.getPackRegistry().registerPack(pack);
				System.out.println("\tPack: " + pack.getId() + " : \"" + pack.getName() + "\" registered.");
				packCount++;
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println(packCount + " packs registered.");
	}
}
