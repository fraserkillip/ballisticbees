package com.fraserkillip.ballisticbees.pack.resources;

import org.newdawn.slick.Image;

import com.fraserkillip.ballisticbees.BallisticBees;
import com.fraserkillip.ballisticbees.entity.paddle.EntityPaddle;

public class PaddleResource extends Resource<EntityPaddle> {
	private int width, height, textureOffsetX, textureOffsetY;
	private String image, sound;

	public PaddleResource(String uid, int width, int height, int textureOffsetX, int textureOffsetY, String image, String sound) {
		super(uid, EntityPaddle.class);
		
		this.width = width;
		this.height = height;
		
		this.textureOffsetX = textureOffsetX;
		this.textureOffsetY = textureOffsetY;
		
		this.image = image;
		this.sound = sound;
	}

	@Override
	public EntityPaddle getInstance() {
		try {
			EntityPaddle paddle = createInstance(new Class<?>[] {int.class, int.class, Image.class, String.class}, new Object[] {width, height, BallisticBees.getImageRegistry().getImage(image), sound});
			paddle.setTextureOffsetX(textureOffsetX);
			paddle.setTextureOffsetY(textureOffsetY);
			return paddle;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
}
