package com.fraserkillip.ballisticbees.pack.resources;

import java.lang.reflect.InvocationTargetException;

import com.fraserkillip.ballisticbees.level.Level;
import com.fraserkillip.ballisticbees.pack.definitions.LevelDefinition;
import com.fraserkillip.ballisticbees.pack.definitions.LevelDefinition.LevelRowOrder;
import com.fraserkillip.ballisticbees.pack.definitions.LevelDefinition.LevelType;
import com.fraserkillip.ballisticbees.pack.definitions.LevelDefinition.LevelWidth;

public class LevelResource extends Resource<Level> {

	private LevelDefinition levelDef;
	
	public LevelResource(String uid, LevelDefinition levelDef) {
		super(uid, Level.class);
		
		this.levelDef = levelDef;
	}

	@Override
	public Level getInstance() {
		// 	public Level(LevelType type, LevelWidth width, LevelRowOrder rowOrder, int rowsOnScreen, int rowsToPlay, String[][] rowDef) {

		try {
			return createInstance(new Class<?>[] { LevelType.class, LevelWidth.class, LevelRowOrder.class, int.class, int.class, String[][].class }, new Object[]{levelDef.getType(), levelDef.getWidth(), levelDef.getRowOrder(), levelDef.getRowsOnScreen(), levelDef.getRowsToPlay(), levelDef.getRows()});
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

}
