package com.fraserkillip.ballisticbees.pack.resources;

import java.lang.reflect.InvocationTargetException;

import org.newdawn.slick.Image;

public class ImageResource extends Resource<Image> {

	private Image img = null;
	
	public ImageResource(String uid, String filepath) {
		super(uid, Image.class);
		try {
			img = createInstance(new Class<?>[]{String.class}, new Object[]{filepath});
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.getCause().printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public Image getInstance() {
		return img;
	}

}
