package com.fraserkillip.ballisticbees.pack.resources;

import java.lang.reflect.InvocationTargetException;

public abstract class Resource<T> {

	private final String uid;
	private final Class<? extends T> clazz;

	protected Resource(String uid, Class<? extends T> clazz) {
		this.uid = uid;
		this.clazz = clazz;
	}

	public String getUid() {
		return uid;
	}

	protected T createInstance(Class<?>[] argTypes, Object[] args) throws InstantiationException, IllegalAccessException, IllegalArgumentException, SecurityException, InvocationTargetException, NoSuchMethodException {
		return (T) clazz.getConstructor(argTypes).newInstance(args);
	}
	
	public abstract T getInstance();
}
