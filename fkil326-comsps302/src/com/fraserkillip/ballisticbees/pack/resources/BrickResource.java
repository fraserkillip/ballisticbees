package com.fraserkillip.ballisticbees.pack.resources;

import com.fraserkillip.ballisticbees.entity.brick.EntityBrick;

public class BrickResource extends Resource<EntityBrick> {
	private static int id = 0;
	private int width, height, textureOffsetX, textureOffsetY;
	private String[] images;
	private String sound;

	public BrickResource(String uid, int width, int height, int textureOffsetX, int textureOffsetY, String[] images, String sound) {
		super(uid, EntityBrick.class);

		this.width = width;
		this.height = height;

		this.textureOffsetX = textureOffsetX;
		this.textureOffsetY = textureOffsetY;

		this.images = images;
		this.sound = sound;
	}

	@Override
	public EntityBrick getInstance() {
		// 	public EntityBrick(int id, int width, int height, Image[] images)
		try {
			EntityBrick brick = createInstance(new Class<?>[] { int.class, int.class, int.class, String[].class, String.class }, new Object[] { id++, width, height, images, sound});
			brick.setTextureOffsetX(textureOffsetX);
			brick.setTextureOffsetY(textureOffsetY);
			return brick;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

}
