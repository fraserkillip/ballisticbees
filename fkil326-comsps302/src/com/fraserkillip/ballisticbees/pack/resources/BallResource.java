package com.fraserkillip.ballisticbees.pack.resources;

import org.newdawn.slick.Image;

import com.fraserkillip.ballisticbees.BallisticBees;
import com.fraserkillip.ballisticbees.entity.ball.EntityBall;

public class BallResource extends Resource<EntityBall> {
	private int radius, textureOffsetX, textureOffsetY;
	private float speed;
	private String image;

	public BallResource(String uid, int radius, float speed, int textureOffsetX, int textureOffsetY, String image) {
		super(uid, EntityBall.class);
		
		this.radius = radius;
		
		this.speed = speed;
		
		this.textureOffsetX = textureOffsetX;
		this.textureOffsetY = textureOffsetY;
		
		this.image = image;
	}

	@Override
	public EntityBall getInstance() {
		// 	public EntityBrick(int id, int width, int height, Image[] images)
		try {
			EntityBall ball = createInstance(new Class<?>[] { int.class, float.class, Image.class }, new Object[] {radius, speed, BallisticBees.getImageRegistry().getImage(image)});
			ball.setTextureOffsetX(textureOffsetX);
			ball.setTextureOffsetY(textureOffsetY);
			
			return ball;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

}
