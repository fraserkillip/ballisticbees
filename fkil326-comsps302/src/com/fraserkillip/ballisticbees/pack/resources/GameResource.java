package com.fraserkillip.ballisticbees.pack.resources;

import com.fraserkillip.ballisticbees.pack.definitions.GameDefinition;

public class GameResource extends Resource<GameDefinition> {

	private final GameDefinition gameDef;
	
	public GameResource(String uid, GameDefinition gameDef) {
		super(uid, GameDefinition.class);
		
		this.gameDef = gameDef;
	}

	@Override
	public GameDefinition getInstance() {
		// TODO Auto-generated method stub
		return gameDef;
	}

}
