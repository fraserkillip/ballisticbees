package com.fraserkillip.ballisticbees.pack.resources;

import java.lang.reflect.InvocationTargetException;

import org.newdawn.slick.Sound;
import org.newdawn.slick.loading.LoadingList;

public class SoundResource extends Resource<Sound> {

	private Sound sound = null;

	public SoundResource(String uid, String filepath) {
		super(uid, Sound.class);
		try {
			LoadingList.setDeferredLoading(false);
			sound = createInstance(new Class<?>[] { String.class }, new Object[] { filepath });
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.getCause().printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public Sound getInstance() {
		return sound;
	}

}
