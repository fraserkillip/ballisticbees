package com.fraserkillip.ballisticbees.pack.definitions;

public class PaddleDefinition extends Definition {

	private int height;

	private int width;
	
	private int textureOffsetX = 0;

	private int textureOffsetY = 0;

	private String image;
	
	private String bouceSound;

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public String getImage() {
		return image;
	}
	
	public int getTextureOffsetX() {
		return textureOffsetX;
	}

	public int getTextureOffsetY() {
		return textureOffsetY;
	}
	
	public String getBounceSound() {
		return bouceSound;
	}

}
