package com.fraserkillip.ballisticbees.pack.definitions;

public class BrickDefinition extends Definition {

	private int height;

	private int width;

	private String[] images;

	private String breakSound;

	private int textureOffsetX = 0;

	private int textureOffsetY = 0;

	public int getTextureOffsetX() {
		return textureOffsetX;
	}

	public int getTextureOffsetY() {
		return textureOffsetY;
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public String[] getImages() {
		return images;
	}

	public String getBreakSound() {
		return breakSound;
	}
}
