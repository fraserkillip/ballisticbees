package com.fraserkillip.ballisticbees.pack.definitions;

public class BallDefinition extends Definition {

	private float speed;
	
	private String image;
	
	private int radius;
	
	private int textureOffsetX = 0;

	private int textureOffsetY = 0;

	public float getSpeed() {
		return speed;
	}

	public String getImage() {
		return image;
	}

	public int getRadius() {
		return radius;
	}

	public int getTextureOffsetX() {
		return textureOffsetX;
	}

	public int getTextureOffsetY() {
		return textureOffsetY;
	}
	
	
}
