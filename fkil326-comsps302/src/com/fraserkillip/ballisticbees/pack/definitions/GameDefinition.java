package com.fraserkillip.ballisticbees.pack.definitions;

public class GameDefinition extends Definition {
	
	protected String description;
	
	protected String title;
	
	protected boolean multiplayer;
	
	protected boolean network;
	
	protected String[] levels;

	public String getDescription() {
		return description;
	}

	public String getTitle() {
		return title;
	}

	public boolean isMultiplayer() {
		return multiplayer;
	}

	public boolean isNetwork() {
		return network;
	}

	public String[] getLevels() {
		return levels;
	}
}
