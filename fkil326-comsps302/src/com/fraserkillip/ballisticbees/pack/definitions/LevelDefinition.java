package com.fraserkillip.ballisticbees.pack.definitions;

public class LevelDefinition extends Definition {
	
	private LevelType type = LevelType.classic;
	
	private LevelWidth width = LevelWidth.full;
	
	private LevelRowOrder rowOrder = LevelRowOrder.normal;
	
	private int rowsOnScreen = 7;
	
	private int rowsToPlay = 0;
	
	private String[][] rows;
	
	public static enum LevelType {
		classic,
		infinite
	}
	
	public static enum LevelWidth {
		full,
		half
	}
	
	public static enum LevelRowOrder {
		normal,
		random
	}
	
	/* The levels ID is defined elsewhere so needs to be set */
	public void setId(String id) {
		this.id = id;
	}

	public LevelType getType() {
		return type;
	}

	public LevelWidth getWidth() {
		return width;
	}

	public LevelRowOrder getRowOrder() {
		return rowOrder;
	}

	public int getRowsOnScreen() {
		return rowsOnScreen;
	}

	public String[][] getRows() {
		return rows;
	}

	public int getRowsToPlay() {
		return rowsToPlay;
	}
}
