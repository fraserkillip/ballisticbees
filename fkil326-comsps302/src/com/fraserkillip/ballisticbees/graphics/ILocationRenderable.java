package com.fraserkillip.ballisticbees.graphics;

import org.newdawn.slick.geom.Vector2f;

/**
 * Defines a object that can be both rendered and moved to a location
 * 
 * @author fraserkillip
 *
 */
public interface ILocationRenderable extends IRenderable {
	
	public void setLocation(Vector2f location);
	
	public Vector2f getLocation();
	
}
