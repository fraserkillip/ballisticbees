package com.fraserkillip.ballisticbees.graphics.helper;

import java.util.ArrayList;

import org.newdawn.slick.Graphics;

/**
 * Provides a facility to manage  graphics states 
 * 
 * @author fraserkillip
 *
 */
public class GraphicsHelper {

	// A list of maintained states
	private static ArrayList<GraphicsState> stateList;
	
	static {
		stateList = new ArrayList<GraphicsState>();
	}
	
	// Push the state
	public static void pushState(Graphics g) {
		stateList.add(new GraphicsState(g));
	}
	
	// Pop the state
	public static void popState(Graphics g) {
		if(stateList.size() > 0) {
			GraphicsState state = stateList.get(stateList.size() - 1);
			state.restoreState(g);
			stateList.remove(state);
		}
	}

}
