package com.fraserkillip.ballisticbees.graphics.helper;

import org.newdawn.slick.Color;

/**
 * Colours used by the game
 * 
 * @author fraserkillip
 *
 */
public class Colours {

	public static Color modalOverlayBG = new Color(0xAA000000);
	
	public static Color headlineFont = new Color(0xFFFF8400);
	
}
