package com.fraserkillip.ballisticbees.graphics.gui;

public interface ButtonListener {
	public void buttonPressed(int id);
}
