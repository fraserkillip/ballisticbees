package com.fraserkillip.ballisticbees.graphics.gui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.gui.GUIContext;

import com.fraserkillip.ballisticbees.graphics.IRenderable;
import com.fraserkillip.ballisticbees.graphics.font.MainFont;
import com.fraserkillip.ballisticbees.graphics.helper.GraphicsHelper;

/**
 * A component with a draggable handle
 * 
 * @author fraserkillip
 *
 */
public class Slider extends BaseComponent implements IRenderable {

	private int max;
	private int min;
	private int value;

	// Defines the x position and width of the handle
	private int handleX = 0;
	private int handleWidth = 30;

	private boolean dragging = false;
	
	private SliderListener listener = null;

	public Slider(GUIContext container, int id, int x, int y, int width, int height, int max, int min) {
		super(container, id, "", x, y, width, height);

		this.max = max;
		this.min = min;

		// Set the x to the side + 2 so it is not clipping with the border
		this.handleX = x + 2;
	}

	@Override
	public void draw(float x, float y, Graphics g) {
		// x and y will always be zero here

		GraphicsHelper.pushState(g);
		
		if (currentFont == null) currentFont = MainFont.font20;

		// Cache if the mouse is over
		boolean mouseIsOver = isMouseOver();

		// // Draw the base
		g.setColor(Color.black);
		g.fillRect(getX(), getY(), getWidth(), getHeight());

		// Draw the BG
		g.setColor(new Color(0xFF444444));
		g.fillRect(getX() + 2, getY() + 2, getWidth() - 4, getHeight() - 4);

		// Draw the slider
		g.setColor(new Color(0xFFCCCCCC));
		g.fillRect(handleX, getY() + 2, handleWidth - 4, getHeight() - 4);

		g.setColor(new Color(0x55FFFFFF));
		g.fillRect(handleX, getY() + 2, handleWidth - 4, 3); // Top bar
		g.fillRect(handleX, getY() + 5, 3, getHeight() - 10); // Left Bar

		// Draw bottom bevel
		g.setColor(new Color(0x55000000));
		g.fillRect(handleX, getY() + getHeight() - 4, handleWidth - 4, 2); // Bottom Bar
		g.fillRect(handleX + handleWidth - 6, getY() + 5, 2, getHeight() - 9); // Right bar

		// If the mouse is over draw the purple overlay
		if (mouseIsOver) {
			g.setColor(new Color(0x66929CD0));
			g.fillRect(handleX, getY() + 2, handleWidth - 4, getHeight() - 4);
		}
		
		GraphicsHelper.popState(g);
	}

	// On mouse down, set dragging and move the handle to the mouse
	@Override
	public void mousePressed(int button, int x, int y) {
		super.mousePressed(button, x, y);

		if (isMouseOver()) {
			dragging = true;
			setValueFromMouse(x, y);
		}
	}

	// On mouse up disable dragging
	@Override
	public void mouseReleased(int button, int x, int y) {
		super.mouseReleased(button, x, y);

		dragging = false;
	}

	// If the mouse is dragged, update the handle
	@Override
	public void mouseDragged(int oldx, int oldy, int newx, int newy) {
		super.mouseDragged(oldx, oldy, newx, newy);
		setValueFromMouse(newx, newy);
	}
	
	// Set the value fo the slider and update the position on the handle
	public void setValueFromMouse(int mouseX, int mouseY) {
		// Ensure we are actually dragging it
		if (dragging) {
			// Calculate the percentage
			float percent = (mouseX - getX() - 10) / (float) (getWidth() - 10);
			
			// Clamp it between 0 and 1
			percent = Math.max(0, percent);
			percent = Math.min(1, percent);

			setValue((int) (min + (max - min) * percent));

			handleX = (int) (getX() + getWidth() * percent);
			
			handleX = Math.max(getX() + 2, handleX);
			handleX = Math.min(handleX, getX() + getWidth() - handleWidth + 2);
		}
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
		float percent = (value - min) / (float) ((max - min));
		percent = Math.max(0, percent);
		percent = Math.min(1, percent);

		handleX = (int) (getX() + getWidth() * percent);
		
		handleX = Math.max(getX() + 2, handleX);
		handleX = Math.min(handleX, getX() + getWidth() - handleWidth + 2);
		
		if(listener != null) {
			listener.sliderValueUpdated(id);
		}
	}

	public SliderListener getListener() {
		return listener;
	}

	public void setListener(SliderListener listener) {
		this.listener = listener;
	}

	@Override
	public <T> T dispose() {
		this.listener = null;
		this.setAcceptingInput(false);
		return null;
	}
}
