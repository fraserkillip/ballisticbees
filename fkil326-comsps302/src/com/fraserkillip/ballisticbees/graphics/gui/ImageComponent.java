package com.fraserkillip.ballisticbees.graphics.gui;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.gui.GUIContext;

import com.fraserkillip.ballisticbees.BallisticBees;
import com.fraserkillip.ballisticbees.exceptions.BallisticBeesException;

public class ImageComponent extends BaseComponent {

	private String imageName;

	private boolean clip = false;

	public ImageComponent(GUIContext container, int id, int x, int y, int width, int height, String imageName) {
		super(container, id, "", x, y, width, height);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void draw(float x, float y, Graphics g) {
		Image img = null;
		try {
			img = BallisticBees.getImageRegistry().getImage(imageName);
		} catch (BallisticBeesException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (img != null) {
			if(clip) g.setClip(xPos, yPos, width, height);
			g.drawImage(img, xPos, yPos);
			if(clip) g.clearClip();
		}
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public boolean isClipped() {
		return clip;
	}

	public void setClipped(boolean clip) {
		this.clip = clip;
	}

	@Override
	public <T> T dispose() {
		return null;
	}
}
