package com.fraserkillip.ballisticbees.graphics.gui;

import java.util.ArrayList;
import java.util.Iterator;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.gui.GUIContext;

import com.fraserkillip.ballisticbees.graphics.helper.GraphicsHelper;

public class GuiContainer extends BaseComponent {

	protected ArrayList<BaseComponent> components = new ArrayList<BaseComponent>();
	
	public GuiContainer(GUIContext container, int id, int x, int y, int width, int height) {
		super(container, id, "", x, y, width, height);
	}
	
	public void addComponent(BaseComponent component) {
		component.setLocation(component.getLocation().add(this.getLocation()));
		
		components.add(component);
	}
	
	public void removeComponent(int id) {
		for (Iterator<BaseComponent> iter = components.iterator(); iter.hasNext();) {
			BaseComponent component = (BaseComponent) iter.next();
			
			if(component.id() == id) {
				iter.remove();
			}
		}
	}
	
	public BaseComponent getComponent(int index) {
		index = Math.max(0, index);
		index = Math.min(components.size() - 1, index);
		return components.get(index);
	}

	@Override
	public void draw(float x, float y, Graphics g) {
		GraphicsHelper.pushState(g);
		
		g.setClip(getX(), getY(), width, height);
				
		for(BaseComponent component : components) {
			component.draw(x, y, g);
		}
		
		GraphicsHelper.popState(g);
	}

	@Override
	public <T> T dispose() {
		
		for(BaseComponent component : components) {
			component.dispose();
		}
		
		components.clear();
		
		return null;
	}

}
