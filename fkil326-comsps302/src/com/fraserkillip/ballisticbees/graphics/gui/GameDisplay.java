package com.fraserkillip.ballisticbees.graphics.gui;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.gui.GUIContext;

import com.fraserkillip.ballisticbees.graphics.font.MainFont;
import com.fraserkillip.ballisticbees.graphics.helper.GraphicsHelper;
import com.fraserkillip.ballisticbees.pack.definitions.GameDefinition;

public class GameDisplay extends BaseComponent{
	
	private RadioButton checkButton;
	
	private ToggleButton toggleButton;
	
	private TextField title, numLevels;

	public GameDisplay(GUIContext container, int id, int panelNum, GameDefinition game, RadioButton checkButton) {
		super(container, id, "", 220*panelNum + 20, 20, 200, 330);
		
		checkButton.setHeight(40);
		checkButton.setWidth(120);
		this.checkButton = checkButton;
		
		List<String> vals = new ArrayList<String>();
		vals.add("Single Player");
		vals.add("Two Player");
		
		if(game.isMultiplayer()) toggleButton = new ToggleButton(container, 0, vals, 0, 0, 180, 50);
		
		title = new TextField(container, 0, game.getTitle(), 0, 0, 100, 30, MainFont.font20, Color.white);
		
		numLevels = new TextField(container, 0, game.getLevels().length + " Level" + (game.getLevels().length == 1 ? "" : "s"), 0, 0, 100, 30, MainFont.font20, Color.white);
	}
	
	@Override
	public void draw(float x, float y, Graphics g) {
		checkButton.setLocation(getX() + 40, getY() + getHeight() - 50);
		if(toggleButton != null) toggleButton.setLocation(getX() + 10, getY() + getHeight() - 110);
		title.setLocation(getX() + 25, getY() + 10);
		numLevels.setLocation(getX() + 25, getY() + 60);
		
		
		GraphicsHelper.pushState(g);

		// Background
		g.setColor(new Color(0f, 0f, 0f, 0.4f));
		g.fillRect(getX(), getY(), getWidth(), getHeight());
		
		// Title
		title.draw(x, y, g);
		numLevels.draw(x, y, g);
		
		if(toggleButton != null) toggleButton.draw(0, 0, g);
		
		checkButton.draw(0, 0, g);

		GraphicsHelper.popState(g);
	}

	@Override
	public <T> T dispose() {
		checkButton = (RadioButton) checkButton.dispose();
		toggleButton.dispose();
		return null;
	}
	
	public boolean getMultiplayer() {
		if(toggleButton == null) return false;
		
		return toggleButton.getMessage() == "Two Player";
	}
}
