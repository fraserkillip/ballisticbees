package com.fraserkillip.ballisticbees.graphics.gui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import com.fraserkillip.ballisticbees.graphics.IRenderable;

public class ProgressBar implements IRenderable {

	private float width = 100;
	private float progress = 0;
	
	public ProgressBar(float width, float progress) {
		this.width = width;
		this.progress = progress;
	}
	
	@Override
	public void draw(float x, float y, Graphics g) {
		// TODO Auto-generated method stub
		
		g.setColor(Color.darkGray);
		g.fillRoundRect(x, y, width, 20, 10);
		
		g.setColor(Color.cyan);
		g.fillRoundRect(x, y, width*progress, 20, 10);
		
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public float getProgress() {
		return progress;
	}

	public void setProgress(float progress) {
		this.progress = progress;
	}

}
