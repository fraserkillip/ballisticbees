package com.fraserkillip.ballisticbees.graphics.gui;

/**
 * Defines interface for receiving updates from a Slider
 * 
 * @author fraserkillip
 *
 */
public interface SliderListener {
	public void sliderValueUpdated(int id);
}
