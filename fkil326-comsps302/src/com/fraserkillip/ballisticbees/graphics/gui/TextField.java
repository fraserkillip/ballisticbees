package com.fraserkillip.ballisticbees.graphics.gui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.gui.GUIContext;

import com.fraserkillip.ballisticbees.graphics.IFadeable;
import com.fraserkillip.ballisticbees.graphics.helper.GraphicsHelper;

/**
 * Defines a string of text to be drawn on screen
 * 
 * @author fraserkillip
 *
 */
public class TextField extends BaseComponent implements IFadeable{

	private Color color;
	
	public TextField(GUIContext container, int id, String message, int x, int y, int width, int height, Font font, Color color) {
		super(container, id, message, x, y, width, height);
		
		this.currentFont = font;
		
		this.color = color.scaleCopy(1.0f);
	}	

	@Override
	public void draw(float x, float y, Graphics g) {
		GraphicsHelper.pushState(g);
		
		g.setFont(currentFont);
		g.setColor(color);
		g.drawString(getMessage(), getX(), getY());
				
		GraphicsHelper.popState(g);
	}

	@Override
	public float getOpacity() {
		return color.a;
	}

	@Override
	public void setOpacity(float opacity) {
		color.a = opacity;
	}

	@Override
	public <T> T dispose() {
		// TODO Auto-generated method stub
		return null;
	}

}
