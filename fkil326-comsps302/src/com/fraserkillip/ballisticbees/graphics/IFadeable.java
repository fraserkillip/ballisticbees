package com.fraserkillip.ballisticbees.graphics;

/**
 * Defines an object that can have opacity set
 * 
 * @author fraserkillip
 *
 */

public interface IFadeable extends IRenderable {
	// Opacity between 0 and 1
	public float getOpacity();
	
	// Opacity between 0 and 1
	public void setOpacity(float opacity);
}
