package com.fraserkillip.ballisticbees.graphics.render;

import org.newdawn.slick.Graphics;

import com.fraserkillip.ballisticbees.GameInfo;
import com.fraserkillip.ballisticbees.common.ITickable;
import com.fraserkillip.ballisticbees.entity.cloud.EntityCloud;
import com.fraserkillip.ballisticbees.graphics.IRenderable;

/**
 * Singleton class that draws the background and maintains state
 * 
 * @author fraserkillip
 *
 */
public class Background implements IRenderable, ITickable {

	private static Background instance;
	
	public static EntityCloud[] clouds;

	private Background() {
		clouds = new EntityCloud[10];
		for(int i = 0; i < clouds.length; i++) {
			clouds[i] = new EntityCloud();
		}
	}

	// Get the instance
	public static Background getInstance() {
		if (instance == null) {
			instance = new Background();
		}

		return instance;
	}

	@Override
	public void draw(float x, float y, Graphics g) {
		Images.skyBackground.draw(0, 0);

		Images.ground.draw(0, GameInfo.GAME_HEIGHT - Images.ground.getHeight());
		
		for(int i = 0; i < clouds.length; i++) {
			if(clouds[i] != null) {
				clouds[i].draw(g);
			}
		}
		
		Images.hive.setAlpha(0.6f);
		Images.hive.draw(GameInfo.GAME_WIDTH - 300, 320, 0.75f);
	}

	@Override
	public void tick(float delta) {
		for(int i = 0; i < clouds.length; i++) {
			if(clouds[i] != null) {
				clouds[i].tick(delta);
				
				if(clouds[i].getPosition().x > GameInfo.GAME_WIDTH) {
					clouds[i].reset();
				}
			}
		}
	}
}
