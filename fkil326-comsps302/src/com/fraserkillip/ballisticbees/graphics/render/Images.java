package com.fraserkillip.ballisticbees.graphics.render;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.loading.LoadingList;

import com.fraserkillip.ballisticbees.GameInfo;


/**
 * Support class contains static references to all game images
 * 
 * @author fraserkillip
 *
 */
public class Images {
	// Common
	public static Image logo;

	// Background
	public static Image skyBackground;
	public static Image ground;
	public static Image[] clouds;
	public static Image hive;

	// Initialise the images
	public static void initImages() throws SlickException {
		LoadingList.setDeferredLoading(true);

		// Common
		Images.logo = new Image(GameInfo.IMAGE_PATH + "Logo.png");

		// Background
		Images.skyBackground = new Image(GameInfo.IMAGE_PATH + "background.png");
		Images.ground = new Image(GameInfo.IMAGE_PATH + "ground.png");
		Images.clouds = new Image[5];
		for (int i = 0; i < Images.clouds.length; i++) {
			try {
				Images.clouds[i] = new Image(GameInfo.IMAGE_PATH + "clouds/cloud" + i + ".png");
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
		Images.hive = new Image(GameInfo.IMAGE_PATH + "hive.png");
	}
}
