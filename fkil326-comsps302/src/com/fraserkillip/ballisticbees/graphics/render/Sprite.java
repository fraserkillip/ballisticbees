package com.fraserkillip.ballisticbees.graphics.render;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Shape;

import com.fraserkillip.ballisticbees.BallisticBees;
import com.fraserkillip.ballisticbees.graphics.IFadeable;


/**
 * Defines an object that has an image, and a bounding box
 * 
 * @author fraserkillip
 *
 */
public class Sprite implements IFadeable {

	protected Image image;

	protected Shape boundingBox;
	
	private int textureOffsetX = 0;

	private int textureOffsetY = 0;

	public Sprite(Image image, Shape shape) {
		this.image = image;
		this.boundingBox = shape;
	}

	@Override
	public void draw(float x, float y, Graphics g) {
		// If we aren't hiding sprites, draw the image at the specified location
		if(!BallisticBees.hideSprites) g.drawImage(getImage(), x + textureOffsetX, y + textureOffsetY);
		
		// If we are showing bounding boxes, draw it
		if(BallisticBees.showBB){
			g.setColor(Color.black);
			g.draw(getBoundingBox());
		}
	}

	public boolean doesIntersect(Shape other) {
		return this.getBoundingBox().intersects(other);
	}
	
	
	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public Shape getBoundingBox() {
		return boundingBox;
	}

	public void setBoundingBox(Shape shape) {
		this.boundingBox = shape;
	}

	@Override
	public float getOpacity() {
		// TODO Auto-generated method stub
		return image.getAlpha();
	}

	@Override
	public void setOpacity(float opacity) {
		image.setAlpha(opacity);
	}

	public int getTextureOffsetX() {
		return textureOffsetX;
	}

	public void setTextureOffsetX(int textureOffsetX) {
		this.textureOffsetX = textureOffsetX;
	}

	public int getTextureOffsetY() {
		return textureOffsetY;
	}

	public void setTextureOffsetY(int textureOffsetY) {
		this.textureOffsetY = textureOffsetY;
	}
}
