package com.fraserkillip.ballisticbees.graphics.fx;

import java.util.ArrayList;

import org.newdawn.slick.Graphics;

public class CompoundEffect extends Effect {

	private ArrayList<Effect> effects;
	
	
	private boolean completeAll;
	
	public CompoundEffect(boolean completeAll, Effect... effectsToAdd) {
		effects = new ArrayList<Effect>();
			
		for(Effect effect : effectsToAdd) {
			effects.add(effect);
		}
		
		this.completeAll = completeAll;
	}

	@Override
	public void draw(float x, float y, Graphics g) {
		for(Effect effect : effects) {
			if(effect.doDraw())	effect.draw(x, y, g);
		}
	}

	@Override
	public void start() {
		for(Effect effect : effects) {
			effect.start();
		}
	}

	@Override
	public void pause() {
		for(Effect effect : effects) {
			effect.pause();
		}
	}

	@Override
	public void complete(boolean stopChain) {
		for(Effect effect : effects) {
			effect.complete(stopChain);
		}
	}

	@Override
	public void stop(boolean stopChain) {
		for(Effect effect : effects) {
			effect.stop(stopChain);
		}
	}

	@Override
	public boolean isFinished(boolean checkQueued) {
		boolean finished = super.isFinished(false);
		for(Effect effect : effects) {
			finished &= effect.isFinished(checkQueued);
		}
		
		if(checkQueued) {
			for (Effect effect : getQueued()) {
				finished &= effect.isFinished(checkQueued);
			}
		}
		
		return finished;
	}
	
	@Override
	public void setFinished(boolean isFinished) {
		super.setFinished(isFinished);
		
		for(Effect effect : effects) {
			effect.setFinished(isFinished);
		}
	}

	@Override
	public void tick(float delta) {
		if(!isStarted() && isFinished(false)) return;

		boolean allFinished = true;
		boolean someFinished = false;
		for(Effect effect : effects) {
			effect.tick(delta);
			boolean effectFinished = effect.isFinished(true);
			allFinished &= effectFinished;
			someFinished |= effectFinished;
		}
		
		if ((completeAll && allFinished) || (!completeAll && someFinished)) {
			didFinish();
		}
	}	
}
