package com.fraserkillip.ballisticbees.graphics.fx;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;

import com.fraserkillip.ballisticbees.graphics.ILocationRenderable;

public class MoveEffect extends Effect {

	private ILocationRenderable item;
	
	private Vector2f initialPosition;
	
	private Vector2f finalPosition;
	
	private Vector2f currentPosition;
	
	private float period;
	
	private float currentTime;
	
	public MoveEffect(ILocationRenderable item, Vector2f initialPosition, Vector2f finalPosition, float period) {
		this.item = item;
		
		this.initialPosition = initialPosition.copy();
		
		this.currentPosition = initialPosition.copy();
		
		this.finalPosition = finalPosition.copy();
		
		this.period = period;
	}

	@Override
	public void draw(float x, float y, Graphics g) {
		item.draw(x, y, g);
	}

	@Override
	public void tick(float delta) {
		if(!isStarted() && isFinished(false)) return;
		
		currentTime += delta;
		
		if(currentTime > period) {
			currentTime = period;
			didFinish();
		}
		
		float percent = getTimingFunction().getValue(currentTime/period);
		
		Vector2f deltaPosition = finalPosition.copy().sub(initialPosition);
		
		Vector2f scaledDelta = deltaPosition.scale(percent);
		
		currentPosition = initialPosition.copy().add(scaledDelta);
		
		item.setLocation(currentPosition);
	}

	@Override
	public void complete(boolean stopChain) {
		
	}

	@Override
	public void stop(boolean stopChain) {
		
	}

}
