package com.fraserkillip.ballisticbees.graphics.fx;

import org.newdawn.slick.Graphics;

import com.fraserkillip.ballisticbees.graphics.IFadeable;
import com.fraserkillip.ballisticbees.graphics.helper.GraphicsHelper;

public class FadeEffect extends Effect {

	private IFadeable item;
	
	private float startOpacity;
	
	private float finalOpacity;
	
	private float currentOpacity;
	
	private float period;
	
	private float currentTime = 0;
		
	public FadeEffect(IFadeable item, float startOpacity, float finalOpacity, float period) {
		this.item = item;
		
		this.startOpacity = startOpacity;
		this.finalOpacity = finalOpacity;
		
		this.period = period;
	}

	@Override
	public void draw(float x, float y, Graphics g) {
		GraphicsHelper.pushState(g);
		
		item.draw(x, y, g);
		
		GraphicsHelper.popState(g);
	}

	@Override
	public void tick(float delta) {
		if(!isStarted() && isFinished(false)) return;
		
		currentTime += delta;
		
		if(currentTime > period) {
			currentTime = period;
			didFinish();
		}
		
		float percent = getTimingFunction().getValue(currentTime/period);
		
		currentOpacity = finalOpacity - startOpacity * percent + startOpacity;
		
		item.setOpacity(currentOpacity);
	}

	@Override
	public void complete(boolean stopChain) {
		// TODO Auto-generated method stub

	}

	@Override
	public void stop(boolean stopChain) {
		// TODO Auto-generated method stub

	}

}
