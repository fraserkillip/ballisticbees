package com.fraserkillip.ballisticbees;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.GameState;
import org.newdawn.slick.state.StateBasedGame;

import com.fraserkillip.ballisticbees.audio.GameMusic;
import com.fraserkillip.ballisticbees.game.Game;
import com.fraserkillip.ballisticbees.graphics.render.Images;
import com.fraserkillip.ballisticbees.pack.registry.BallRegistry;
import com.fraserkillip.ballisticbees.pack.registry.BrickRegistry;
import com.fraserkillip.ballisticbees.pack.registry.GameRegistry;
import com.fraserkillip.ballisticbees.pack.registry.ImageRegistry;
import com.fraserkillip.ballisticbees.pack.registry.LevelRegistry;
import com.fraserkillip.ballisticbees.pack.registry.PackRegistry;
import com.fraserkillip.ballisticbees.pack.registry.PaddleRegistry;
import com.fraserkillip.ballisticbees.pack.registry.SoundRegistry;
import com.fraserkillip.ballisticbees.states.StatePlaying;
import com.fraserkillip.ballisticbees.states.States;

/**
 * Main game class
 * 
 * 
 * @author fraserkillip
 * 
 */
public class BallisticBees extends StateBasedGame {

	public static boolean showBB = false; // Show bounding boxes
	public static boolean hideSprites = false; // Hides sprite images
	public static boolean autoPlay = false; // Control the paddle automatically
	public static boolean showPhysicsDebug = false; // Show physics information
	public static boolean showAIDebug = false;
	public static float physicsSpeed = 1.0f; // Scale the physics speed

	private boolean f3KeyDown = false; // Store whether the F3 key is currently down

	// Store the instance of the game
	public static BallisticBees instance;

	// Registries
	private static final PackRegistry packRegistry = new PackRegistry();
	private static final ImageRegistry imageRegistry = new ImageRegistry();
	private static final SoundRegistry soundRegistry = new SoundRegistry();

	private static final GameRegistry gameRegistry = new GameRegistry();
	private static final LevelRegistry levelRegistry = new LevelRegistry();

	private static final BallRegistry ballRegistry = new BallRegistry();
	private static final BrickRegistry brickRegistry = new BrickRegistry();
	private static final PaddleRegistry paddleRegistry = new PaddleRegistry();

	private static Settings settings = null;

	public static Settings getSettings() {
		return settings;
	}

	// Constructor
	public BallisticBees(String name) {
		super(name);

		instance = this;

		// Initialise resources
		try {
			Images.initImages();
			GameMusic.initMusic();
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Load settings from file
		try {
			FileInputStream fileIn = new FileInputStream("settings.dat");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			settings = (Settings) in.readObject();
			in.close();
			fileIn.close();
		} catch (IOException i) {
		} catch (ClassNotFoundException c) {
		}

		// Settings could not be found so create a new one
		if (settings == null) {
			settings = new Settings();
		}
		
		saveSettings();
	}

	public static void saveSettings() {
		try {
			FileOutputStream fileOut = new FileOutputStream("settings.dat");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(settings);
			out.close();
			fileOut.close();
		} catch (IOException i) {
			i.printStackTrace();
		}
	}

	// Initialised all game states
	@Override
	public void initStatesList(GameContainer container) throws SlickException {
		States.initStates(this);

		// Transition into first state
		this.enterState(States.STATE_SPLASH.getID());
	}

	@Override
	public void keyReleased(int key, char c) {
		super.keyReleased(key, c);

		// Track whether the F3 key was released
		if (key == Input.KEY_F3) {
			f3KeyDown = false;
		}
	}

	@Override
	public void keyPressed(int key, char c) {
		super.keyPressed(key, c);

		// Set F3 key down for developer options
		if (key == Input.KEY_F3) {
			f3KeyDown = true;
			return;
		}

		// If the F3 key is down then switch on the key to enable dev options
		if (f3KeyDown) {
			switch (key) {
				case Input.KEY_B:
					showBB = !showBB;
					break;
				case Input.KEY_F:
					getContainer().setShowFPS(!getContainer().isShowingFPS());
					break;
				case Input.KEY_S:
					hideSprites = !hideSprites;
					break;
				case Input.KEY_A:
					autoPlay = !autoPlay;
					break;
				case Input.KEY_P:
					showPhysicsDebug = !showPhysicsDebug;
					break;
				case Input.KEY_I:
					showAIDebug = !showAIDebug;
					break;
				case Input.KEY_T:
					// FIXME: fullscreen is bugged in Library
					//					try {
					//						getContainer().setFullscreen(!getContainer().isFullscreen());
					//					} catch (SlickException e) {
					//						// TODO Auto-generated catch block
					//						e.printStackTrace();
					//					}
					break;

				// Set the physics speed
				case Input.KEY_0:
					physicsSpeed = 1;
					break;
				case Input.KEY_1:
					physicsSpeed = 0.1F;
					break;
				case Input.KEY_2:
					physicsSpeed = 0.5F;
					break;
				case Input.KEY_3:
					physicsSpeed = 2F;
					break;
				case Input.KEY_4:
					physicsSpeed = 4F;
					break;
			}
		}
	}

	// Get the currently running game if we are playing
	public Game getCurrentGame() {
		GameState state = getCurrentState();

		if (state instanceof StatePlaying) { return ((StatePlaying) state).getGame(); }

		return null;
	}

	public static PackRegistry getPackRegistry() {
		return packRegistry;
	}

	public static ImageRegistry getImageRegistry() {
		return imageRegistry;
	}

	public static LevelRegistry getLevelRegistry() {
		return levelRegistry;
	}

	public static BallRegistry getBallRegistry() {
		return ballRegistry;
	}

	public static BrickRegistry getBrickRegistry() {
		return brickRegistry;
	}

	public static PaddleRegistry getPaddleRegistry() {
		return paddleRegistry;
	}

	public static GameRegistry getGameRegistry() {
		return gameRegistry;
	}

	public static SoundRegistry getSoundRegistry() {
		return soundRegistry;
	}
}
