package com.fraserkillip.ballisticbees.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class Listener {

	protected Object obj;
	protected Method method;
	
	protected Listener(Object obj, Method method) {
		this.obj = obj;
		this.method = method;
	}
	
	protected EventPropagation execute(Object object) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		return (EventPropagation) method.invoke(obj, object);
	}

}
