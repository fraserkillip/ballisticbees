package com.fraserkillip.ballisticbees.event;

import java.lang.reflect.Method;
import java.util.ArrayList;

import com.fraserkillip.ballisticbees.entity.Entity;
import com.fraserkillip.ballisticbees.entity.ball.EntityBall;
import com.fraserkillip.ballisticbees.entity.brick.EntityBrick;
import com.fraserkillip.ballisticbees.entity.paddle.EntityPaddle;
import com.fraserkillip.ballisticbees.entity.powerup.EntityPowerup;
import com.fraserkillip.ballisticbees.exceptions.IllegalEventListenerException;
import com.fraserkillip.ballisticbees.game.Game;
import com.fraserkillip.ballisticbees.game.powerup.Powerup;
import com.fraserkillip.ballisticbees.level.Level;

public class EventEngine {

	private static ArrayList<Object> subscribers;

	static {
		subscribers = new ArrayList<Object>();
	}

	/*
	 * Subscribe Unsubscribe
	 */

	public static void subscribe(Object obj) throws IllegalEventListenerException {
		boolean valid = false;

		if (subscribers.contains(obj)) return;

		Class<?> clazz = obj.getClass();

		for (Method method : clazz.getMethods()) {
			EventListener listener = method.getAnnotation(EventListener.class);

			if (listener == null) continue;

			Class<?>[] params = method.getParameterTypes();

			// We only take one parameter
			if (params.length != 1) { throw new IllegalEventListenerException(); }

			// The parameter must have an event type
			if (!Event.class.isAssignableFrom(params[0])) { throw new IllegalEventListenerException(); }

			// We return void
			if (method.getReturnType() != EventPropagation.class) { throw new IllegalEventListenerException(); }

			try {
				@SuppressWarnings("unchecked")
				ArrayList<Listener> listeners = (ArrayList<Listener>) params[0].getDeclaredField("listeners").get(params[0]);
				listeners.add(new Listener(obj, method));
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			}

			valid = true;
		}

		if (valid) {
			subscribers.add(obj);
		}
	}

	public static void unsubscribe(Object obj) throws IllegalEventListenerException {
		if (!subscribers.contains(obj)) return;

		Class<?> clazz = obj.getClass();

		for (Method method : clazz.getMethods()) {
			EventListener listenerAnnotation = method.getAnnotation(EventListener.class);

			if (listenerAnnotation == null) continue;

			Class<?>[] params = method.getParameterTypes();

			// We only take one parameter
			if (params.length != 1) { throw new IllegalEventListenerException(); }

			// The parameter must have an event type
			if (!Event.class.isAssignableFrom(params[0])) { throw new IllegalEventListenerException(); }

			// We return void
			if (method.getReturnType() != EventPropagation.class) { throw new IllegalEventListenerException(); }

			try {
				@SuppressWarnings("unchecked")
				ArrayList<Listener> listeners = (ArrayList<Listener>) params[0].getSuperclass().getDeclaredField("listeners").get(params[0]);
				@SuppressWarnings("unchecked")
				ArrayList<Listener> toRemove = (ArrayList<Listener>) params[0].getSuperclass().getDeclaredField("removedListeners").get(params[0]);
				for (Listener listener : listeners) {
					if (listener.obj == obj) {
						toRemove.add(listener);
					}
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			}
		}
		subscribers.remove(obj);
	}

	public static void raiseBrickDamageEvent(EntityBrick brick, EntityBall ball) {
		BrickWillDamageEvent willEvent = new BrickWillDamageEvent(brick, ball);

		switch (willEvent.raise()) {
			case CANCEL:
			case STOP:
				return;
			case CONTINUE:
			case FORCE_CONTINUE:
				break;
		}

		BrickDidDamageEvent didEvent = new BrickDidDamageEvent(brick, ball);
		didEvent.raise();
	}
	
	public static void raiseBrickBreakEvent(EntityBrick brick) {
		BrickWillBreakEvent willEvent = new BrickWillBreakEvent(brick);

		switch (willEvent.raise()) {
			case CANCEL:
			case STOP:
				return;
			case CONTINUE:
			case FORCE_CONTINUE:
				break;
		}

		BrickDidBreakEvent didEvent = new BrickDidBreakEvent(brick);
		didEvent.raise();
	}
	
	public static void raiseBallHitPaddleEvent(EntityPaddle paddle, EntityBall ball) {
		BallWillHitPaddleEvent willEvent = new BallWillHitPaddleEvent(paddle, ball);

		switch (willEvent.raise()) {
			case CANCEL:
			case STOP:
				return;
			case CONTINUE:
			case FORCE_CONTINUE:
				break;
		}

		BallDidHitPaddleEvent didEvent = new BallDidHitPaddleEvent(paddle, ball);
		didEvent.raise();
	}
	
	public static void raiseBallHitFloorEvent(EntityBall ball) {
		BallWillHitFloorEvent willEvent = new BallWillHitFloorEvent(ball);

		switch (willEvent.raise()) {
			case CANCEL:
			case STOP:
				return;
			case CONTINUE:
			case FORCE_CONTINUE:
				break;
		}

		BallDidHitFloorEvent didEvent = new BallDidHitFloorEvent(ball);
		didEvent.raise();
	}
	
	public static void raiseBallHitBallEvent(EntityBall ballA, EntityBall ballB) {
		BallWillHitBallEvent willEvent = new BallWillHitBallEvent(ballA, ballB);

		switch (willEvent.raise()) {
			case CANCEL:
			case STOP:
				return;
			case CONTINUE:
			case FORCE_CONTINUE:
				break;
		}

		BallDidHitBallEvent didEvent = new BallDidHitBallEvent(ballA, ballB);
		didEvent.raise();
	}
	
	public static void raiseEntitySpawnEvent(Entity entity) {
		EntityWillSpawnEvent willEvent = new EntityWillSpawnEvent(entity);

		switch (willEvent.raise()) {
			case CANCEL:
			case STOP:
				return;
			case CONTINUE:
			case FORCE_CONTINUE:
				break;
		}

		EntityDidSpawnEvent didEvent = new EntityDidSpawnEvent(entity);
		didEvent.raise();
	}
	
	public static void raiseEntityDespawnEvent(Entity entity) {
		EntityWillDespawnEvent willEvent = new EntityWillDespawnEvent(entity);

		switch (willEvent.raise()) {
			case CANCEL:
			case STOP:
				return;
			case CONTINUE:
			case FORCE_CONTINUE:
				break;
		}

		EntityDidDespawnEvent didEvent = new EntityDidDespawnEvent(entity);
		didEvent.raise();
	}
	
	public static void raisePowerupHitPaddleEvent(EntityPowerup powerup, EntityPaddle paddle) {
		PowerupWillHitPaddleEvent willEvent = new PowerupWillHitPaddleEvent(powerup, paddle);

		switch (willEvent.raise()) {
			case CANCEL:
			case STOP:
				return;
			case CONTINUE:
			case FORCE_CONTINUE:
				break;
		}

		PowerupDidHitPaddleEvent didEvent = new PowerupDidHitPaddleEvent(powerup, paddle);
		didEvent.raise();
	}
	
	public static void raisePowerupActivateEvent(Powerup powerup) {
		PowerupWillActivateEvent willEvent = new PowerupWillActivateEvent(powerup);

		switch (willEvent.raise()) {
			case CANCEL:
			case STOP:
				return;
			case CONTINUE:
			case FORCE_CONTINUE:
				break;
		}

		PowerupDidActivateEvent didEvent = new PowerupDidActivateEvent(powerup);
		didEvent.raise();
	}
	
	public static void raisePowerupDeactivateEvent(Powerup powerup) {
		PowerupWillDeactivateEvent willEvent = new PowerupWillDeactivateEvent(powerup);

		switch (willEvent.raise()) {
			case CANCEL:
			case STOP:
				return;
			case CONTINUE:
			case FORCE_CONTINUE:
				break;
		}

		PowerupDidDeactivateEvent didEvent = new PowerupDidDeactivateEvent(powerup);
		didEvent.raise();
	}
	
	public static void raiseGameStartEvent(Game game) {
		GameWillStartEvent willEvent = new GameWillStartEvent(game);

		switch (willEvent.raise()) {
			case CANCEL:
			case STOP:
				return;
			case CONTINUE:
			case FORCE_CONTINUE:
				break;
		}

		GameDidStartEvent didEvent = new GameDidStartEvent(game);
		didEvent.raise();
	}
	
	public static void raiseGameEndEvent(Game game) {
		GameWillEndEvent willEvent = new GameWillEndEvent(game);
		switch (willEvent.raise()) {
			case CANCEL:
			case STOP:
				return;
			case CONTINUE:
			case FORCE_CONTINUE:
				break;
		}

		GameDidEndEvent didEvent = new GameDidEndEvent(game);
		didEvent.raise();
	}
	
	public static void raiseLevelStartEvent(Level level) {
		LevelWillStartEvent willEvent = new LevelWillStartEvent(level);

		switch (willEvent.raise()) {
			case CANCEL:
			case STOP:
				return;
			case CONTINUE:
			case FORCE_CONTINUE:
				break;
		}

		LevelDidStartEvent didEvent = new LevelDidStartEvent(level);
		didEvent.raise();
	}
	
	public static void raiseLevelEndEvent(Level level) {
		LevelWillEndEvent willEvent = new LevelWillEndEvent(level);

		switch (willEvent.raise()) {
			case CANCEL:
			case STOP:
				return;
			case CONTINUE:
			case FORCE_CONTINUE:
				break;
		}

		LevelDidEndEvent didEvent = new LevelDidEndEvent(level);
		didEvent.raise();
	}
}

/*
 * Notes:
 * 
 * Events:
 * 
 * BrickWillDamageEvent: EntityBrick, EntityBall, Game
 * BrickDidDamageEvent: EntityBrick, EntityBall, Game
 * 
 * BrickWillBreakEvent: EntityBrick
 * BrickDidBreakEvent: EntityBrick
 * 
 * BallWillHitPaddleEvent: EntityBall, EntityPaddle
 * BallDidHitPaddleEvent: EntityBall, EntityPaddle
 * 
 * BallWillHitFloorEvent: EntityBall
 * BallDidHitFloorEvent: EntityBall
 * 
 * BallWillCollideWithBallEvent: EntityBall, EntityBalll
 * BallDidCollideWithBallEvent: EntityBall, EntityBalll
 * 
 * EntityWillSpawnEvent: Entity
 * EntityDidSpawnEvent: Entity
 * 
 * EnityWillDespawnEvent: Entity
 * EntityDidDespawnEvent: Entity
 * 
 * PaddleWillHitPowerupEvent: EntityPaddle, EntityPowerup
 * PaddleDidHitPowerupEvent: EntityPaddle, EntityPowerup
 * 
 * PowerupWillActivateEvent: Powerup
 * PowerupDidActivateEvent: Powerup
 * 
 * PowerupWillDeactivateEvent: Powerup
 * PowerupDidDeactivateEvent: Powerup
 * 
 * GameWillStartEvent: Game
 * GameDidStartEvent: Game
 * 
 * GameWillEndEvent: Game
 * GameDidEndEvent: Game
 * 
 * LevelWillStartEvent: Level
 * LevelDidStartEvent: Level
 * 
 * LevelWillEndEvent: Level
 * LevelDidEndEvent: Level
 * 
 * ClientDidConnectEvent
 * 
 * ClientDidDisconnectEvent
 */