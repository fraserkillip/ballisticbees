package com.fraserkillip.ballisticbees.event;

public enum EventPriority {
	HIGH,
	MEDIUM,
	LOW
}
