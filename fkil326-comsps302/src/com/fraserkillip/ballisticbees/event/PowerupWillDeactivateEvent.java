package com.fraserkillip.ballisticbees.event;

import java.util.ArrayList;

import com.fraserkillip.ballisticbees.game.powerup.Powerup;

public class PowerupWillDeactivateEvent extends Event {

	protected static ArrayList<Listener> listeners;
	protected static ArrayList<Listener> removedListeners;
	
	static {
		listeners = new ArrayList<Listener>();
		removedListeners = new ArrayList<Listener>();
	}
	
	private Powerup powerup;

	protected PowerupWillDeactivateEvent(Powerup powerup) {
		this.powerup = powerup;
	}

	@Override
	protected EventPropagation raise() {
		return invoke(this);
	}
	
	public Powerup getPowerup() {
		return powerup;
	}

	@Override
	protected ArrayList<Listener> getListeners() {
		// TODO Auto-generated method stub
		return listeners;
	}

	@Override
	protected ArrayList<Listener> getRemovedListeners() {
		// TODO Auto-generated method stub
		return removedListeners;
	}

}
