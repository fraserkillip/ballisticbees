package com.fraserkillip.ballisticbees.event;

import java.util.ArrayList;

import com.fraserkillip.ballisticbees.entity.ball.EntityBall;
import com.fraserkillip.ballisticbees.entity.paddle.EntityPaddle;

public class BallDidHitPaddleEvent extends Event {

	protected static ArrayList<Listener> listeners;
	protected static ArrayList<Listener> removedListeners;
	
	static {
		listeners = new ArrayList<Listener>();
		removedListeners = new ArrayList<Listener>();
	}
	
	private EntityPaddle paddle;
	private EntityBall ball;

	protected BallDidHitPaddleEvent(EntityPaddle paddle, EntityBall ball) {
		this.paddle = paddle;
		this.ball = ball;
	}

	@Override
	protected EventPropagation raise() {
		return invoke(this);
	}
	
	public EntityPaddle getBrick() {
		return paddle;
	}
	
	public EntityBall getBall() {
		return ball;
	}

	@Override
	protected ArrayList<Listener> getListeners() {
		// TODO Auto-generated method stub
		return listeners;
	}

	@Override
	protected ArrayList<Listener> getRemovedListeners() {
		// TODO Auto-generated method stub
		return removedListeners;
	}

}
