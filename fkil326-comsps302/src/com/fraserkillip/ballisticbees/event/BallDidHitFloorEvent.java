package com.fraserkillip.ballisticbees.event;

import java.util.ArrayList;

import com.fraserkillip.ballisticbees.entity.ball.EntityBall;

public class BallDidHitFloorEvent extends Event {

	protected static ArrayList<Listener> listeners;
	protected static ArrayList<Listener> removedListeners;
	
	static {
		listeners = new ArrayList<Listener>();
		removedListeners = new ArrayList<Listener>();
	}
	
	private EntityBall ball;

	protected BallDidHitFloorEvent(EntityBall ball) {
		this.ball = ball;
	}

	@Override
	protected EventPropagation raise() {
		return invoke(this);
	}
	
	public EntityBall getBall() {
		return ball;
	}

	@Override
	protected ArrayList<Listener> getListeners() {
		// TODO Auto-generated method stub
		return listeners;
	}

	@Override
	protected ArrayList<Listener> getRemovedListeners() {
		// TODO Auto-generated method stub
		return removedListeners;
	}

}
