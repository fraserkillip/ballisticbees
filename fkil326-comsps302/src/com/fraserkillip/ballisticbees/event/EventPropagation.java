package com.fraserkillip.ballisticbees.event;

public enum EventPropagation {
	/**
	 * The event propagation should continue normally.
	 */
	CONTINUE,
	
	/**
	 * The event propagation should not continue
	 */
	STOP,
	
	/**
	 * Override other EventResponseType.STOP responses and continue normally
	 */
	FORCE_CONTINUE,
	
	/**
	 * Forcefully stop propagation of the event, will override EventResponseType.FORCE_CONTINUE
	 */
	CANCEL
}
