package com.fraserkillip.ballisticbees.event;

import java.util.ArrayList;

import com.fraserkillip.ballisticbees.level.Level;

public class LevelWillStartEvent extends Event {

	protected static ArrayList<Listener> listeners;
	protected static ArrayList<Listener> removedListeners;
	
	static {
		listeners = new ArrayList<Listener>();
		removedListeners = new ArrayList<Listener>();
	}
	
	private Level level;

	protected LevelWillStartEvent(Level level) {
		this.level = level;
	}

	@Override
	protected EventPropagation raise() {
		return invoke(this);
	}
	
	public Level getLevel() {
		return level;
	}

	@Override
	protected ArrayList<Listener> getListeners() {
		// TODO Auto-generated method stub
		return listeners;
	}

	@Override
	protected ArrayList<Listener> getRemovedListeners() {
		// TODO Auto-generated method stub
		return removedListeners;
	}

}
