package com.fraserkillip.ballisticbees.event;

import java.util.ArrayList;

import com.fraserkillip.ballisticbees.entity.ball.EntityBall;

public class BallWillHitBallEvent extends Event {

	protected static ArrayList<Listener> listeners;
	protected static ArrayList<Listener> removedListeners;
	
	static {
		listeners = new ArrayList<Listener>();
		removedListeners = new ArrayList<Listener>();
	}
	
	private EntityBall ballA;
	private EntityBall ballB;

	protected BallWillHitBallEvent(EntityBall ballA, EntityBall ballB) {
		this.ballA = ballA;
		this.ballB = ballB;
	}

	@Override
	protected EventPropagation raise() {
		return invoke(this);
	}
	
	public EntityBall getBallA() {
		return ballA;
	}
	
	public EntityBall getBallB() {
		return ballB;
	}

	@Override
	protected ArrayList<Listener> getListeners() {
		// TODO Auto-generated method stub
		return listeners;
	}

	@Override
	protected ArrayList<Listener> getRemovedListeners() {
		// TODO Auto-generated method stub
		return removedListeners;
	}

}
