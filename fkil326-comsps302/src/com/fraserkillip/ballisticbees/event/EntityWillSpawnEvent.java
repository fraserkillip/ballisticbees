package com.fraserkillip.ballisticbees.event;

import java.util.ArrayList;

import com.fraserkillip.ballisticbees.entity.Entity;

public class EntityWillSpawnEvent extends Event {

	protected static ArrayList<Listener> listeners;
	protected static ArrayList<Listener> removedListeners;
	
	static {
		listeners = new ArrayList<Listener>();
		removedListeners = new ArrayList<Listener>();
	}
	
	private Entity entity;

	protected EntityWillSpawnEvent(Entity entity) {
		this.entity = entity;
	}

	@Override
	protected EventPropagation raise() {
		return invoke(this);
	}
	
	public Entity getEntity() {
		return entity;
	}

	@Override
	protected ArrayList<Listener> getListeners() {
		// TODO Auto-generated method stub
		return listeners;
	}

	@Override
	protected ArrayList<Listener> getRemovedListeners() {
		// TODO Auto-generated method stub
		return removedListeners;
	}

}
