package com.fraserkillip.ballisticbees.event;

import java.util.ArrayList;

import com.fraserkillip.ballisticbees.entity.ball.EntityBall;
import com.fraserkillip.ballisticbees.entity.brick.EntityBrick;

public class BrickDidDamageEvent extends Event {

	protected static ArrayList<Listener> listeners;
	protected static ArrayList<Listener> removedListeners;
	
	static {
		listeners = new ArrayList<Listener>();
		removedListeners = new ArrayList<Listener>();
	}
	
	private EntityBrick brick;
	private EntityBall ball;

	protected BrickDidDamageEvent(EntityBrick brick, EntityBall ball) {
		this.brick = brick;
		this.ball = ball;
	}

	@Override
	protected EventPropagation raise() {
		return invoke(this);
	}
	
	public EntityBrick getBrick() {
		return brick;
	}
	
	public EntityBall getBall() {
		return ball;
	}

	@Override
	protected ArrayList<Listener> getListeners() {
		// TODO Auto-generated method stub
		return listeners;
	}

	@Override
	protected ArrayList<Listener> getRemovedListeners() {
		// TODO Auto-generated method stub
		return removedListeners;
	}
}
