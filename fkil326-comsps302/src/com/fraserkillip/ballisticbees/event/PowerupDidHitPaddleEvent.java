package com.fraserkillip.ballisticbees.event;

import java.util.ArrayList;

import com.fraserkillip.ballisticbees.entity.paddle.EntityPaddle;
import com.fraserkillip.ballisticbees.entity.powerup.EntityPowerup;

public class PowerupDidHitPaddleEvent extends Event {

	protected static ArrayList<Listener> listeners;
	protected static ArrayList<Listener> removedListeners;
	
	static {
		listeners = new ArrayList<Listener>();
		removedListeners = new ArrayList<Listener>();
	}
	
	private EntityPowerup powerup;
	private EntityPaddle paddle;

	protected PowerupDidHitPaddleEvent(EntityPowerup powerup, EntityPaddle ball) {
		this.powerup = powerup;
		this.paddle = ball;
	}

	@Override
	protected EventPropagation raise() {
		return invoke(this);
	}
	
	public EntityPowerup getPowerup() {
		return powerup;
	}
	
	public EntityPaddle getPaddle() {
		return paddle;
	}

	@Override
	protected ArrayList<Listener> getListeners() {
		// TODO Auto-generated method stub
		return listeners;
	}

	@Override
	protected ArrayList<Listener> getRemovedListeners() {
		// TODO Auto-generated method stub
		return removedListeners;
	}

}
