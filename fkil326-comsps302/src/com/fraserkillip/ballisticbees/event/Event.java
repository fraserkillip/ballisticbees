package com.fraserkillip.ballisticbees.event;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public abstract class Event {
	
	protected EventPropagation invoke(Event event) {
		EventPropagation response = EventPropagation.CONTINUE;
		
		// Remove the listeners before we loop them
		getListeners().removeAll(getRemovedListeners());
		getRemovedListeners().clear();
		
		for(Listener listener : getListeners()) {
			try {
				EventPropagation methodResponse = (EventPropagation) listener.execute(event);
				
				// If we force stop return immediately
				if(methodResponse == EventPropagation.CANCEL) {
					return EventPropagation.CANCEL;
				}
				
				if(response == EventPropagation.CONTINUE && methodResponse == EventPropagation.STOP) {
					response = EventPropagation.STOP;
				}
				
				if(methodResponse == EventPropagation.FORCE_CONTINUE) {
					response = EventPropagation.FORCE_CONTINUE;
				}
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return response;
	}
	
	protected abstract EventPropagation raise();
	
	protected abstract ArrayList<Listener> getListeners();
	protected abstract ArrayList<Listener> getRemovedListeners();
}

/*
Notes:

Events:

BrickWillDamageEvent: EntityBrick, EntityBall, Game
BrickDidDamageEvent: EntityBrick, EntityBall, Game

BrickWillBreakEvent: EntityBrick
BrickDidBreakEvent: EntityBrick

BallWillHitPaddleEvent: EntityBall, EntityPaddle
BallDidHitPaddleEvent: EntityBall, EntityPaddle

BallWillHitFloorEvent: EntityBall
BallDidHitFloorEvent: EntityBall

BallWillCollideWithBallEvent: EntityBall, EntityBalll
BallDidCollideWithBallEvent: EntityBall, EntityBalll

EntityWillSpawnEvent: Entity
EntityDidSpawnEvent: Entity

EnityWillDespawnEvent: Entity
EntityDidDespawnEvent: Entity

PaddleWillHitPowerupEvent: EntityPaddle, EntityPowerup
PaddleDidHitPowerupEvent: EntityPaddle, EntityPowerup

PowerupWillActivateEvent: Powerup
PowerupDidActivateEvent: Powerup

PowerupWillDeactivateEvent: Powerup
PowerupDidDeactivateEvent: Powerup

GameWillStartEvent: Game
GameDidStartEvent: Game

GameWillEndEvent: Game
GameDidEndEvent: Game

LevelWillStartEvent: Level
LevelDidStartEvent: Level

LevelWillEndEvent: Level
LevelDidEndEvent: Level

ClientDidConnectEvent

ClientDidDisconnectEvent

*/