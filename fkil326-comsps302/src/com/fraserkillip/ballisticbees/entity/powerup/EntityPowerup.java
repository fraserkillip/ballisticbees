package com.fraserkillip.ballisticbees.entity.powerup;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Vector2f;

import com.fraserkillip.ballisticbees.BallisticBees;
import com.fraserkillip.ballisticbees.entity.CollidableEntity;
import com.fraserkillip.ballisticbees.entity.paddle.EntityPaddle;
import com.fraserkillip.ballisticbees.event.EventEngine;
import com.fraserkillip.ballisticbees.event.EventListener;
import com.fraserkillip.ballisticbees.event.EventPropagation;
import com.fraserkillip.ballisticbees.event.PowerupDidHitPaddleEvent;
import com.fraserkillip.ballisticbees.exceptions.IllegalEventListenerException;
import com.fraserkillip.ballisticbees.game.powerup.Powerup;
import com.fraserkillip.ballisticbees.physics.BodyDef;
import com.fraserkillip.ballisticbees.physics.ICollidable;

public class EntityPowerup extends CollidableEntity {

	private Powerup currentPowerup;
	
	public EntityPowerup(int xPos, int yPos, Powerup powerup) {
		super(BallisticBees.getImageRegistry().getImage("vanilla:honey_drop"), new Circle(xPos, yPos, powerup.getIcon().getWidth() / 2));
		this.setVelocity(new Vector2f(0, 75f));
		
		setPosition(new Vector2f(xPos, yPos));
		
		setCurrentPowerup(powerup);
		
		setTextureOffsetX(-5);
		setTextureOffsetY(-10);
		
		
		try {
			EventEngine.subscribe(this);
		} catch (IllegalEventListenerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void draw(float x, float y, Graphics g) {
		super.draw(x, y, g);
		
		g.drawImage(currentPowerup.getIcon(), getPosition().x, getPosition().y);
	}
	
	@Override
	public boolean shouldCollideWith(ICollidable target) {
		return target instanceof EntityPaddle;
	}

	@Override
	public BodyDef getBodyDef() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void didCollideWith(ICollidable target) {}

	public Powerup getCurrentPowerup() {
		return currentPowerup;
	}

	public void setCurrentPowerup(Powerup currentPowerup) {
		this.currentPowerup = currentPowerup;
	}
	
	@EventListener
	public EventPropagation powerupHitPaddleEvent(PowerupDidHitPaddleEvent event) {
		if(event.getPowerup() == this) this.setDead();
		return EventPropagation.CONTINUE;
	}
}
