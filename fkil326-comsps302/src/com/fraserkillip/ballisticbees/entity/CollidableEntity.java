package com.fraserkillip.ballisticbees.entity;

import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Shape;

import com.fraserkillip.ballisticbees.physics.ICollidable;

public abstract class CollidableEntity extends Entity implements ICollidable{

	protected boolean isActive = true;
	
	public CollidableEntity(Image image, Shape shape) {
		super(image, shape);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean getActive() {
		// TODO Auto-generated method stub
		return isActive;
	}

	@Override
	public void setActive(boolean active) {
		isActive = active;
	}
}
