package com.fraserkillip.ballisticbees.entity.cloud;

import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

import com.fraserkillip.ballisticbees.GameInfo;
import com.fraserkillip.ballisticbees.entity.Entity;
import com.fraserkillip.ballisticbees.graphics.render.Images;

public class EntityCloud extends Entity {

	public EntityCloud() {
		// Spawn the entity off screen with no bounding box
		super(Images.clouds[(int)(Math.random()*Images.clouds.length)], new Rectangle(0, 0, 0, 0));
		
		reset();
		
		// Place some on screen when creating
		setPosition(new Vector2f((float)(Math.random() * GameInfo.GAME_WIDTH * 2 - GameInfo.GAME_WIDTH), (float)(Math.random() * 200 + 20)));
	}
	
	public void reset() {
		float distance = (float)Math.random() * 0.30f + 0.30f;
		
		setImage(getImage().getScaledCopy(distance));
		
		setVelocity(new Vector2f((float)(25 * (distance - 0.3) + 1), 0));
		
		setPosition(new Vector2f((float)(Math.random() * GameInfo.GAME_WIDTH/2 - GameInfo.GAME_WIDTH/2), (float)(Math.random() * 200 + 20)));
		
		image.setAlpha((float)Math.random() * 0.30f + 0.40f);
	}

}
