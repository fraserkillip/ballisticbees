package com.fraserkillip.ballisticbees.entity.ball;

import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Vector2f;

import com.fraserkillip.ballisticbees.entity.CollidableEntity;
import com.fraserkillip.ballisticbees.entity.brick.EntityBrick;
import com.fraserkillip.ballisticbees.entity.paddle.EntityPaddle;
import com.fraserkillip.ballisticbees.physics.BodyDef;
import com.fraserkillip.ballisticbees.physics.ICollidable;

public class EntityBall extends CollidableEntity {

	private float speed;
	
	public EntityBall(int radius, float speed, Image image) {
		super(image, new Circle(0, 0, radius));
		this.speed = speed;
	}

	public void damage() {
	};

	@Override
	public void tick(float delta) {
		// TODO Auto-generated method stub
		super.tick(delta);
	}

	@Override
	public void setDead() {
		// TODO Auto-generated method stub
		super.setDead();

		this.velocity = new Vector2f(0, 0);
	}

	@Override
	public boolean shouldCollideWith(ICollidable target) {
		return target instanceof EntityPaddle || target instanceof EntityBrick;
	}

	@Override
	public BodyDef getBodyDef() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void didCollideWith(ICollidable target) {
		// TODO Auto-generated method stub

	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}
}
