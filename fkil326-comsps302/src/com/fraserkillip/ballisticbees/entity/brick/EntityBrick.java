package com.fraserkillip.ballisticbees.entity.brick;

import java.util.Random;
import java.util.regex.Pattern;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.RoundedRectangle;

import com.fraserkillip.ballisticbees.BallisticBees;
import com.fraserkillip.ballisticbees.entity.CollidableEntity;
import com.fraserkillip.ballisticbees.entity.ball.EntityBall;
import com.fraserkillip.ballisticbees.event.BrickDidDamageEvent;
import com.fraserkillip.ballisticbees.event.BrickWillDamageEvent;
import com.fraserkillip.ballisticbees.event.EventEngine;
import com.fraserkillip.ballisticbees.event.EventListener;
import com.fraserkillip.ballisticbees.event.EventPropagation;
import com.fraserkillip.ballisticbees.exceptions.IllegalEventListenerException;
import com.fraserkillip.ballisticbees.graphics.font.MainFont;
import com.fraserkillip.ballisticbees.graphics.helper.GraphicsHelper;
import com.fraserkillip.ballisticbees.physics.BodyDef;
import com.fraserkillip.ballisticbees.physics.ICollidable;

public class EntityBrick extends CollidableEntity {

	private static final Random random = new Random();

	private int brickId;

	private int lives;
	private String[] images, sounds;

	private Image currentImage;

	private static Pattern pipe = Pattern.compile("|", Pattern.LITERAL);

	public EntityBrick(int id, int width, int height, String[] images, String sound) {
		super(BallisticBees.getImageRegistry().getImage("vanilla:blank"), new RoundedRectangle(0, 0, width, height, 10F));
		setBrickId(id);
		this.images = images;
		this.setLives(images.length);
		try {
			EventEngine.subscribe(this);
		} catch (IllegalEventListenerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		updateImage();
		
		sounds = pipe.split(sound);
	}

	@Override
	public void draw(Graphics g) {
		super.draw(this.position.x, this.position.y, g);

		if (BallisticBees.hideSprites) {
			GraphicsHelper.pushState(g);

			g.setFont(MainFont.font20);
			g.setColor(Color.black);
			g.drawString("" + lives, getPosition().x + getBoundingBox().getWidth() / 2, getPosition().y);

			GraphicsHelper.popState(g);
		}
	}

	public int getLives() {
		return lives;
	}

	public void setLives(int lives) {
		this.lives = lives;
	}

	@Override
	public Image getImage() {
		return currentImage;
	}

	public void damage() {
		BallisticBees.getSoundRegistry().getSound(sounds[random.nextInt(sounds.length)]).play();
		if (--lives == 0) {
			setDead();
		} else {
			updateImage();
		}
	}

	@Override
	public void setDead() {
		super.setDead();
		setActive(false);
		EventEngine.raiseBrickBreakEvent(this);
	}

	@Override
	public boolean shouldCollideWith(ICollidable target) {
		return target instanceof EntityBall;
	}

	@Override
	public void didCollideWith(ICollidable target) {
	}

	@EventListener
	public EventPropagation willDamage(BrickWillDamageEvent event) {
		if (event.getBrick() != this) return EventPropagation.CONTINUE;
		return EventPropagation.CONTINUE;
	}

	@EventListener
	public EventPropagation didDamage(BrickDidDamageEvent event) {
		if (event.getBrick() != this) return EventPropagation.CONTINUE;
		damage();
		return EventPropagation.CONTINUE;
	}

	@Override
	public BodyDef getBodyDef() {
		// TODO Auto-generated method stub
		return null;
	}

	public int getBrickId() {
		return brickId;
	}

	public void setBrickId(int brickId) {
		this.brickId = brickId;
	}

	public void updateImage() {
		String[] imgSet = pipe.split(images[lives - 1]);

		currentImage = BallisticBees.getImageRegistry().getImage(imgSet[random.nextInt(imgSet.length)]);
	}
}
