package com.fraserkillip.ballisticbees.entity;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;

import com.fraserkillip.ballisticbees.common.ITickable;
import com.fraserkillip.ballisticbees.graphics.ILocationRenderable;
import com.fraserkillip.ballisticbees.graphics.render.Sprite;

public class Entity extends Sprite implements ITickable, ILocationRenderable {

	protected Vector2f position = new Vector2f(0, 0);

	// pixels/s
	protected Vector2f velocity = new Vector2f(0, 0);

	// pixels/s/s
	protected Vector2f acceleration = new Vector2f(0, 0);

	private boolean dead;
	
	public Entity(Image image, Shape shape) {
		super(image, shape);
	}

	public void draw(Graphics g) {
		draw(position.x, position.y, g);
	};
	
	@Override
	public void draw(float x, float y, Graphics g) {
		super.draw(x, y, g);
	}

	public void rotateVelocity(double theta) {
		velocity.add(theta);
	}

	public void rotateAcceleration(double theta) {
		acceleration.add(theta);
	}

	@Override
	public void tick(float delta) {
		velocity.add(acceleration.copy().scale((float) (delta / 1000.0)));
		setPosition(position.add(velocity.copy().scale((float) (delta / 1000.0))));
	}

	public Vector2f getPosition() {
		return position;
	}

	public void setPosition(Vector2f position) {
		this.position = position;
		boundingBox.setLocation(position);
	}

	public Vector2f getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector2f velocity) {
		this.velocity = velocity;
	}

	public Vector2f getAcceleration() {
		return acceleration;
	}

	public void setAcceleration(Vector2f acceleration) {
		this.acceleration = acceleration;
	};
	
	public boolean isDead() {
		return dead;
	}

	public void setDead() {
		this.dead = true;
	}

	@Override
	public void setLocation(Vector2f location) {
		this.position = location.copy();
	}

	@Override
	public Vector2f getLocation() {
		return position;
	}
}
