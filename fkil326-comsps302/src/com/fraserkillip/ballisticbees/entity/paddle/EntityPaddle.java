package com.fraserkillip.ballisticbees.entity.paddle;

import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Curve;
import org.newdawn.slick.geom.Vector2f;

import com.fraserkillip.ballisticbees.BallisticBees;
import com.fraserkillip.ballisticbees.entity.CollidableEntity;
import com.fraserkillip.ballisticbees.entity.ball.EntityBall;
import com.fraserkillip.ballisticbees.entity.powerup.EntityPowerup;
import com.fraserkillip.ballisticbees.event.BallDidHitPaddleEvent;
import com.fraserkillip.ballisticbees.event.EventEngine;
import com.fraserkillip.ballisticbees.event.EventListener;
import com.fraserkillip.ballisticbees.event.EventPropagation;
import com.fraserkillip.ballisticbees.exceptions.IllegalEventListenerException;
import com.fraserkillip.ballisticbees.physics.BodyDef;
import com.fraserkillip.ballisticbees.physics.ICollidable;

public class EntityPaddle extends CollidableEntity {

	private String breakSound = null;
	
	public EntityPaddle(int width, int height, Image image, String sound) {
		super(image, new Curve(new Vector2f(0, height/2), new Vector2f(width / 6, -height/5), new Vector2f(5 * width / 6, -height/5), new Vector2f(width, height/2)));
		this.breakSound = sound;
		try {
			EventEngine.subscribe(this);
		} catch (IllegalEventListenerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void ballDidCollide(EntityBall ball) {
		if (ball.getPosition().y + ball.getBoundingBox().getHeight() > this.getPosition().y + 5) {
			ball.setDead();
			return;
		}
	}

	@Override
	public boolean shouldCollideWith(ICollidable target) {
		return target instanceof EntityBall || target instanceof EntityPowerup;
	}

	@Override
	public BodyDef getBodyDef() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void didCollideWith(ICollidable target) {
		// TODO Auto-generated method stub
	}
	
	@EventListener
	public EventPropagation ballHitPaddle(BallDidHitPaddleEvent event) {
		if(breakSound != null) BallisticBees.getSoundRegistry().getSound(breakSound).play();		
		return EventPropagation.CONTINUE;
	}
}
