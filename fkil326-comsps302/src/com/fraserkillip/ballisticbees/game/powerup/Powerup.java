package com.fraserkillip.ballisticbees.game.powerup;

import org.newdawn.slick.Image;

import com.fraserkillip.ballisticbees.common.ITickable;
import com.fraserkillip.ballisticbees.game.Game;
import com.fraserkillip.ballisticbees.graphics.IRenderable;

public abstract class Powerup implements ITickable, IRenderable {
	protected Game game;
	
	// Activate the powerup
	public abstract void activate();
	
	// Deactivate the powerup
	public abstract void deactivate();
	
	// Does this powerup draw an overlay on the screen
	public abstract boolean hasOverlay();
	
	// Has the powerup finished working
	public abstract boolean isComplete();
	
	// The icon for the falling entity
	public abstract Image getIcon();
	
	// Set the game so whe know what to modify
	public void setGame(Game game) {
		this.game = game;
	}
}
