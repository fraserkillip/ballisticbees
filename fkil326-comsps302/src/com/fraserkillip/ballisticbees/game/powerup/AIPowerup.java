package com.fraserkillip.ballisticbees.game.powerup;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.fraserkillip.ballisticbees.BallisticBees;
import com.fraserkillip.ballisticbees.entity.paddle.EntityPaddle;

public class AIPowerup extends Powerup {

	private float timeRemaining = 10000;
	
	private EntityPaddle hitPaddle;

	public EntityPaddle getHitPaddle() {
		return hitPaddle;
	}

	public void setHitPaddle(EntityPaddle hitPaddle) {
		this.hitPaddle = hitPaddle;
	}

	@Override
	public void tick(float delta) {
	}

	@Override
	public void draw(float x, float y, Graphics g) {
	}

	@Override
	public void activate() {
		game.enableAI(hitPaddle, timeRemaining);
	}

	@Override
	public void deactivate() {
	}

	@Override
	public boolean hasOverlay() {
		return false;
	}

	@Override
	public boolean isComplete() {
		return timeRemaining< 0;
	}

	@Override
	public Image getIcon() {
		// TODO Auto-generated method stub
		return BallisticBees.getImageRegistry().getImage("vanilla:ai_powerup");
	}

}
