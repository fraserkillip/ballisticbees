package com.fraserkillip.ballisticbees.game.powerup;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.fraserkillip.ballisticbees.BallisticBees;

public class BonusScorePowerup extends Powerup {

	@Override
	public void tick(float delta) {}

	@Override
	public void draw(float x, float y, Graphics g) {}

	@Override
	public void activate() {
		game.addScore(100, true);
	}

	@Override
	public void deactivate() {}

	@Override
	public boolean hasOverlay() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isComplete() {
		return true;
	}

	@Override
	public Image getIcon() {
		// TODO Auto-generated method stub
		return BallisticBees.getImageRegistry().getImage("vanilla:bonus_powerup");
	}

}
