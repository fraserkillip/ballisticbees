package com.fraserkillip.ballisticbees.game.powerup;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.fraserkillip.ballisticbees.BallisticBees;
import com.fraserkillip.ballisticbees.entity.paddle.EntityPaddle;

public class LargerPaddlePowerup extends Powerup {

	private float timeRemaining = 10000;
	
	private EntityPaddle oldPaddle, newPaddle;
	
	public LargerPaddlePowerup() {
		newPaddle = BallisticBees.getPaddleRegistry().getPaddle("vanilla:paddle_large");
	}
	
	public EntityPaddle getOldPaddle() {
		return oldPaddle;
	}

	public void setOldPaddle(EntityPaddle oldPaddle) {
		this.oldPaddle = oldPaddle;
	}

	@Override
	public void tick(float delta) {
		timeRemaining -= delta;
	}

	@Override
	public void draw(float x, float y, Graphics g) {
	}

	@Override
	public void activate() {
		game.swapPaddle(newPaddle, oldPaddle);
	}

	@Override
	public void deactivate() {
		game.revertPaddle(newPaddle);
	}

	@Override
	public boolean hasOverlay() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isComplete() {
		// TODO Auto-generated method stub
		return timeRemaining < 0;
	}

	@Override
	public Image getIcon() {
		// TODO Auto-generated method stub
		return BallisticBees.getImageRegistry().getImage("vanilla:larger_powerup");
	}

}
