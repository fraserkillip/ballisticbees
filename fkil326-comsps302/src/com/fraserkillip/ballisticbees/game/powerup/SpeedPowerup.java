package com.fraserkillip.ballisticbees.game.powerup;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.fraserkillip.ballisticbees.BallisticBees;

public class SpeedPowerup extends Powerup {

	// 5 seconds duration
	private float timeRemaining = 10000;
	
	private float speed = 0.5f;
	
	@Override
	public void tick(float delta) {
		timeRemaining -= delta;
	}

	@Override
	public void draw(float x, float y, Graphics g) {
	}

	@Override
	public void activate() {
		game.addGameSpeed(speed);
	}

	@Override
	public void deactivate() {
		game.removeGameSpeed(speed);
	}

	@Override
	public boolean hasOverlay() {
		return false;
	}

	@Override
	public Image getIcon() {
		return BallisticBees.getImageRegistry().getImage("vanilla:speed_powerup");
	}

	@Override
	public boolean isComplete() {
		return timeRemaining <= 0;
	}

}
