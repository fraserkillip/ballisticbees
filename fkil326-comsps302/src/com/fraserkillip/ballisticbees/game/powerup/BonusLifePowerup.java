package com.fraserkillip.ballisticbees.game.powerup;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.fraserkillip.ballisticbees.BallisticBees;

public class BonusLifePowerup extends Powerup {

	@Override
	public void tick(float delta) {
	}

	@Override
	public void draw(float x, float y, Graphics g) {
	}

	@Override
	public void activate() {
		if(game.getRemainingLives() < 3) game.addLife();
	}

	@Override
	public void deactivate() {
	}

	@Override
	public boolean hasOverlay() {
		return false;
	}

	@Override
	public boolean isComplete() {
		return true;
	}

	@Override
	public Image getIcon() {
		return BallisticBees.getImageRegistry().getImage("vanilla:life_powerup");
	}

}
