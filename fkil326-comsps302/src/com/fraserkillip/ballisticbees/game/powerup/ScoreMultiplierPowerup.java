package com.fraserkillip.ballisticbees.game.powerup;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.fraserkillip.ballisticbees.BallisticBees;

public class ScoreMultiplierPowerup extends Powerup {

	private float timeRemaining = 10000;

	@Override
	public void tick(float delta) {
		timeRemaining -= delta;
	}

	@Override
	public void draw(float x, float y, Graphics g) {
	}

	@Override
	public void activate() {
		game.addScoreMultipler(1f);
	}

	@Override
	public void deactivate() {
		game.removeScoreMultipler(1f);

	}

	@Override
	public boolean hasOverlay() {
		return false;
	}

	@Override
	public boolean isComplete() {
		return timeRemaining < 0;
	}

	@Override
	public Image getIcon() {
		return BallisticBees.getImageRegistry().getImage("vanilla:multiplier_powerup");
	}

}
