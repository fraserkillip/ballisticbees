package com.fraserkillip.ballisticbees.game.powerup;

import java.util.HashMap;
import java.util.Random;

public class Powerups {

	private static final Random random = new Random();

	private static HashMap<String, Class<? extends Powerup>> powerups = new HashMap<String, Class<? extends Powerup>>();
	
	// Add the available powerups
	static {
		powerups.put("speed", SpeedPowerup.class);
		powerups.put("score", BonusScorePowerup.class);
		powerups.put("life", BonusLifePowerup.class);
		powerups.put("ball", ExtraBallPowerup.class);
		powerups.put("largePaddle", LargerPaddlePowerup.class);
		powerups.put("multiplier", ScoreMultiplierPowerup.class);
		powerups.put("AI", AIPowerup.class);
	}

	public static Powerup getPowerupByName(String name) {
		try {
			return powerups.get(name).newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public static Powerup getRandomPowerupFromSet(String[] set) {
		return getPowerupByName((set[random.nextInt(set.length)]));
	}
	
	public static Powerup getRandomPowerup() {
		return getRandomPowerupFromSet((String[]) powerups.keySet().toArray(new String[]{}));
	}
}
