package com.fraserkillip.ballisticbees.game;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.KeyListener;
import org.newdawn.slick.MouseListener;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import com.fraserkillip.ballisticbees.BallisticBees;
import com.fraserkillip.ballisticbees.GameInfo;
import com.fraserkillip.ballisticbees.common.ITickable;
import com.fraserkillip.ballisticbees.entity.Entity;
import com.fraserkillip.ballisticbees.entity.ball.EntityBall;
import com.fraserkillip.ballisticbees.entity.paddle.EntityPaddle;
import com.fraserkillip.ballisticbees.entity.powerup.EntityPowerup;
import com.fraserkillip.ballisticbees.event.BallDidHitFloorEvent;
import com.fraserkillip.ballisticbees.event.BrickDidBreakEvent;
import com.fraserkillip.ballisticbees.event.BrickDidDamageEvent;
import com.fraserkillip.ballisticbees.event.EventEngine;
import com.fraserkillip.ballisticbees.event.EventListener;
import com.fraserkillip.ballisticbees.event.EventPropagation;
import com.fraserkillip.ballisticbees.event.GameDidEndEvent;
import com.fraserkillip.ballisticbees.event.PowerupDidHitPaddleEvent;
import com.fraserkillip.ballisticbees.event.PowerupWillHitPaddleEvent;
import com.fraserkillip.ballisticbees.exceptions.BallisticBeesException;
import com.fraserkillip.ballisticbees.exceptions.IllegalEventListenerException;
import com.fraserkillip.ballisticbees.game.powerup.Powerup;
import com.fraserkillip.ballisticbees.game.powerup.Powerups;
import com.fraserkillip.ballisticbees.graphics.IRenderable;
import com.fraserkillip.ballisticbees.graphics.font.MainFont;
import com.fraserkillip.ballisticbees.graphics.fx.CompoundEffect;
import com.fraserkillip.ballisticbees.graphics.fx.Effect;
import com.fraserkillip.ballisticbees.graphics.fx.FadeEffect;
import com.fraserkillip.ballisticbees.graphics.fx.FormattedCounterEffect;
import com.fraserkillip.ballisticbees.graphics.fx.FormattedCounterEffect.CounterSet;
import com.fraserkillip.ballisticbees.graphics.fx.MoveEffect;
import com.fraserkillip.ballisticbees.graphics.gui.Button;
import com.fraserkillip.ballisticbees.graphics.gui.ButtonListener;
import com.fraserkillip.ballisticbees.graphics.gui.TextField;
import com.fraserkillip.ballisticbees.graphics.helper.Colours;
import com.fraserkillip.ballisticbees.graphics.helper.GraphicsHelper;
import com.fraserkillip.ballisticbees.level.Level;
import com.fraserkillip.ballisticbees.math.Easing;
import com.fraserkillip.ballisticbees.pack.definitions.GameDefinition;
import com.fraserkillip.ballisticbees.physics.World;
import com.fraserkillip.ballisticbees.physics.World.PhysicsLayer;
import com.fraserkillip.ballisticbees.player.Player;
import com.fraserkillip.ballisticbees.player.PlayerController;
import com.fraserkillip.ballisticbees.player.PlayerState;
import com.fraserkillip.ballisticbees.states.States;

public class Game implements IRenderable, ITickable, MouseListener, KeyListener, ButtonListener {

	public static enum GameState {
		PRE, PLAYING, PAUSED, GAMEOVER, WIN
	}

	private static final Random random = new Random();

	public static Random getRandom() {
		return random;
	}

	public List<Level> levels = new ArrayList<Level>();
	public Level currentLevel;

	private int levelCounter = 1;

	public EntityPaddle paddle1, paddle2;

	public List<EntityBall> balls = new ArrayList<EntityBall>();

	public int remainingLives = 3;

	public List<Entity> entities = new ArrayList<Entity>();

	public List<Powerup> activePowerups = new ArrayList<Powerup>();

	private List<Effect> currentEffects = new ArrayList<Effect>();

	private float scoreMultiplier = 1;

	public int score = 0;

	public int bonusScore = 0;

	protected World world;

	public GameState currentState = GameState.PRE;

	private GameContainer container;

	private StateBasedGame stateBasedGame;

	private GameDefinition gameDef;

	private Button backToMenuButton;

	private float gameSpeed = 1.0f;

	private TextField winBaseScoreField;

	private TextField winBonusScoreField;

	private TextField winBallsRemainingField;

	private TextField winTotalScoreField;

	private ArrayList<Effect> winScreenEffects;

	private int paddleSwapCounter1 = 0;

	private PlayerController player1, player2;

	private boolean multiplayer;

	private int paddleSwapCounter2;

	public Game(GameContainer container, StateBasedGame stateBasedGame, GameDefinition gameDef, boolean multiplayer) {
		this.container = container;
		this.stateBasedGame = stateBasedGame;
		this.gameDef = gameDef;

		this.multiplayer = multiplayer;

		try {
			EventEngine.subscribe(this);
		} catch (IllegalEventListenerException e) {
			e.printStackTrace();
		}

		backToMenuButton = new Button(container, 1, "MAIN MENU", 300, GameInfo.GAME_HEIGHT - 100, 200, 75);
		backToMenuButton.setListener(this);
		backToMenuButton.setAcceptingInput(false);

		world = new World();

		world.addGameBounds(0, 0, GameInfo.GAME_WIDTH, GameInfo.GAME_HEIGHT);

		setupLevels();

		// Pop the first level
		currentLevel = levels.get(0);
		currentLevel.initRows();
		levels.remove(0);

		showNextLevelText(levelCounter++);

		try {
			paddle1 = BallisticBees.getPaddleRegistry().getPaddle("vanilla:paddle_normal");
			if (multiplayer) paddle2 = BallisticBees.getPaddleRegistry().getPaddle("vanilla:paddle_normal");
		} catch (BallisticBeesException e2) {
			e2.printStackTrace();
		}
		paddle1.setPosition(new Vector2f(200, GameInfo.GAME_HEIGHT - 70));
		if (multiplayer) paddle2.setPosition(new Vector2f(500, GameInfo.GAME_HEIGHT - 70));
		world.registerEntity(paddle1, PhysicsLayer.LAYER_1);
		if (multiplayer) world.registerEntity(paddle2, PhysicsLayer.LAYER_1);

		player1 = new PlayerController(container, Player.Player1, this, paddle1);

		player1.setLoadedBall(addBall(true, paddle1));

		if (multiplayer) {
			player2 = new PlayerController(container, Player.Player2, this, paddle2);

			player2.setLoadedBall(addBall(true, paddle2));
		}
	}

	public EntityBall addBall(boolean stationary, EntityPaddle paddle) {
		EntityBall ball = null;
		try {
			ball = BallisticBees.getBallRegistry().getBall("vanilla:ball_normal");
			float xPos = paddle.getBoundingBox().getCenterX() - ball.getBoundingBox().getWidth() / 2;
			float yPos = paddle.getBoundingBox().getMinY() - ball.getBoundingBox().getHeight();
			ball.setPosition(new Vector2f(xPos, yPos));

			if (!stationary) {
				ball.setVelocity(new Vector2f(0, -420));
				ball.getVelocity().add(getRandom().nextInt(50) - 25);
			}

			world.registerEntity(ball, PhysicsLayer.LAYER_1);
			balls.add(ball);
		} catch (BallisticBeesException e) {
			e.printStackTrace();
		}
		return ball;
	}

	public void addGameSpeed(float add) {
		this.gameSpeed += add;
	}

	public void addLife() {
		remainingLives++;
	}

	// ---- Win View ----

	public void addScore(int amount, boolean bonus) {
		int totalAmount = (int) (amount * scoreMultiplier);
		if (bonus) {
			bonusScore += totalAmount;
		} else {
			score += totalAmount;
		}

		TextField field = new TextField(container, 0, "test", 150, GameInfo.GAME_HEIGHT - 30, 0, 0, MainFont.font25, Color.white);

		FormattedCounterEffect counter = new FormattedCounterEffect("+ %d", field, new CounterSet(0, totalAmount, 200));
		counter.setTimingFunction(Easing.easeOutCubic);
		counter.setDraw(false);
		counter.start();

		FadeEffect fader = new FadeEffect(field, 1, 0, 2000);
		fader.setTimingFunction(Easing.easeOutCubic);
		fader.setDraw(false);
		fader.start();

		MoveEffect mover = new MoveEffect(field, field.getLocation(), new Vector2f(200, GameInfo.GAME_HEIGHT - 30), 2000);
		mover.setTimingFunction(Easing.easeOutCubic);
		mover.start();

		currentEffects.add(new CompoundEffect(true, counter, fader, mover));
	}

	public void addScoreMultipler(float f) {
		scoreMultiplier += f;
	}

	@EventListener
	public EventPropagation ballHitGroundListener(BallDidHitFloorEvent event) {
		event.getBall().setActive(false);
		event.getBall().setDead();

		balls.remove(event.getBall());

		if (balls.size() == 0) {
			if (remainingLives == 0) {
				currentState = GameState.GAMEOVER;
				EventEngine.raiseGameEndEvent(this);
				return EventPropagation.CONTINUE;
			}

			remainingLives--;

			player1.setState(PlayerState.Waiting);
			player1.setLoadedBall(addBall(true, paddle1));

			if (player2 != null) {
				player2.setState(PlayerState.Waiting);
				player2.setLoadedBall(addBall(true, paddle2));
			}
		}

		return EventPropagation.CONTINUE;
	}

	@EventListener
	public EventPropagation brickBrokenListener(BrickDidBreakEvent event) {
		if (currentLevel.isComplete()) {
			if (levels.size() == 0) {
				currentState = GameState.WIN;
				EventEngine.raiseGameEndEvent(this);
			}
		} else {
			if (random.nextInt(15) == 1) {
				Powerup powerup = Powerups.getRandomPowerup();
				powerup.setGame(this);

				EntityPowerup powerupEntity = new EntityPowerup((int) event.getBrick().getBoundingBox().getCenterX(), (int) event.getBrick().getBoundingBox().getCenterY(), powerup);
				world.registerEntity(powerupEntity, PhysicsLayer.LAYER_1);
				entities.add(powerupEntity);
			}
		}

		return EventPropagation.CONTINUE;
	}

	@EventListener
	public EventPropagation brickDidDamageEventListener(BrickDidDamageEvent event) {
		addScore(10, false);
		return EventPropagation.CONTINUE;
	}

	@Override
	public void buttonPressed(int id) {
		switch (id) {
			case 1:
				backToMenuButton = backToMenuButton.dispose();
				stateBasedGame.enterState(States.STATE_MENU.getID());
				break;
		}
	}

	private void clearEntitiesOnLevelEnd() {
		for (EntityBall ball : balls) {
			ball.setDead();
		}
		balls.clear();

		for (Entity e : entities) {
			if (e instanceof EntityPowerup) {
				e.setDead();
			}
		}
	}

	@Override
	public void draw(float x, float y, Graphics g) {
		GraphicsHelper.pushState(g);
		// Draw the bricks
		currentLevel.draw(100, 100, g);

		// Draw the balls
		for (EntityBall ball : balls) {
			ball.draw(g);
		}

		// Draw the other entities
		for (Entity entity : entities) {
			entity.draw(g);
		}

		// Draw the paddle
		paddle1.draw(g);

		if (multiplayer) {
			paddle2.draw(g);
		}

		g.setColor(Color.white);
		g.setFont(MainFont.font25);
		// Draw the GUI
		g.drawString("Score: " + (score + bonusScore), 10, GameInfo.GAME_HEIGHT - 30);

		for (int i = 0; i < remainingLives; i++) {
			try {
				BallisticBees.getImageRegistry().getImage("vanilla:ball_normal").draw(GameInfo.GAME_WIDTH - 50 - 50 * i, GameInfo.GAME_HEIGHT - 40);
			} catch (BallisticBeesException e) {
				e.printStackTrace();
			}
		}

		for (Effect effect : currentEffects) {
			effect.draw(x, y, g);
		}

		if (BallisticBees.showPhysicsDebug) world.draw(0, 0, g);

		switch (currentState) {
			case PAUSED:
				drawPaused(g);
				break;
			case PLAYING:
				break;
			case GAMEOVER:
				drawGameover(g);
				break;
			case PRE:
				break;
			case WIN:
				drawWin(g);
				break;
			default:
				break;
		}

		if (BallisticBees.showAIDebug) {
			player1.drawDebug(g);
			if (player2 != null) {
				player2.drawDebug(g);
			}
		}

		GraphicsHelper.popState(g);
	}

	public void drawGameover(Graphics g) {
		g.setColor(Colours.modalOverlayBG);
		g.fillRect(0, 0, GameInfo.GAME_WIDTH, GameInfo.GAME_HEIGHT);
		g.setColor(Color.white);
		g.setFont(MainFont.font40);
		g.drawString("GAME OVER!", GameInfo.GAME_WIDTH / 2 - MainFont.font40.getWidth("GAME OVER!") / 2, 100);

		for (Effect effect : winScreenEffects) {
			effect.draw(0, 0, g);
		}
		
		backToMenuButton.draw(0, 0, g);

	}

	public void drawPaused(Graphics g) {
		g.setColor(Colours.modalOverlayBG);
		g.fillRect(0, 0, GameInfo.GAME_WIDTH, GameInfo.GAME_HEIGHT);
		g.setColor(Color.white);
		g.setFont(MainFont.font40);
		g.drawString("PAUSED", GameInfo.GAME_WIDTH / 2 - MainFont.font40.getWidth("PAUSED") / 2, 100);
		
		backToMenuButton.draw(0, 0, g);
	}

	public void drawWin(Graphics g) {
		g.setColor(Colours.modalOverlayBG);
		g.fillRect(0, 0, GameInfo.GAME_WIDTH, GameInfo.GAME_HEIGHT);
		g.setColor(Color.white);
		g.setFont(MainFont.font40);
		g.drawString("YOU WIN!", GameInfo.GAME_WIDTH / 2 - MainFont.font40.getWidth("YOU WIN!") / 2, 50);

		for (Effect effect : winScreenEffects) {
			effect.draw(0, 0, g);
		}

		backToMenuButton.draw(0, 0, g);
	}

	public void enableAI(EntityPaddle hitPaddle, float timeRemaining) {
		if (hitPaddle == paddle1) {
			player1.addAITime(timeRemaining);
		}
	}

	@EventListener
	public EventPropagation gameEndedEventListener(GameDidEndEvent event) {
			winScreenEffects = new ArrayList<Effect>();

			winBaseScoreField = new TextField(container, 0, "", GameInfo.GAME_WIDTH / 2 - MainFont.font30.getWidth("Base Score: " + score) / 2, 150, 0, 0, MainFont.font30, Color.white);
			winBonusScoreField = new TextField(container, 0, "", GameInfo.GAME_WIDTH / 2 - MainFont.font30.getWidth("Bonus Score: " + bonusScore) / 2, 200, 0, 0, MainFont.font30, Color.white);
			winBallsRemainingField = new TextField(container, 0, "", GameInfo.GAME_WIDTH / 2 - MainFont.font30.getWidth("Remaining Balls: " + remainingLives + " x 1000 = " + remainingLives * 1000) / 2, 250, 0, 0, MainFont.font30, Color.white);
			winTotalScoreField = new TextField(container, 0, "Total Score: " + (score + bonusScore + (remainingLives * 1000)), GameInfo.GAME_WIDTH / 2 - MainFont.font30.getWidth("Total Score: " + (score + bonusScore + (remainingLives * 1000))) / 2, 300, 0, 0, MainFont.font30, Color.white);

			FormattedCounterEffect baseCounter = new FormattedCounterEffect("Base Score: %d", winBaseScoreField, new CounterSet(0, score, 1000));
			baseCounter.setTimingFunction(Easing.easeOutSine);
			baseCounter.start();

			FormattedCounterEffect totalCounterOne = new FormattedCounterEffect("Total Score: %d", winTotalScoreField, new CounterSet(0, score, 1000));
			totalCounterOne.setTimingFunction(Easing.easeOutSine);
			totalCounterOne.start();

			CompoundEffect stepOne = new CompoundEffect(true, baseCounter, totalCounterOne);

			winScreenEffects.add(stepOne);

			FormattedCounterEffect bonusCounter = new FormattedCounterEffect("Bonus Score: %d", winBonusScoreField, new CounterSet(0, bonusScore, 1000));
			baseCounter.setTimingFunction(Easing.easeOutSine);

			FormattedCounterEffect totalCounterTwo = new FormattedCounterEffect("Total Score: %d", winTotalScoreField, new CounterSet(score, score + bonusScore, 1000));
			totalCounterTwo.setTimingFunction(Easing.easeOutSine);

			CompoundEffect stepTwo = new CompoundEffect(true, bonusCounter, totalCounterTwo);

			stepOne.queueEffect(stepTwo);

			winScreenEffects.add(stepTwo);

			FormattedCounterEffect ballsCounter = new FormattedCounterEffect("Remaining Balls: %d x 1000 = %d", winBallsRemainingField, new CounterSet(0, remainingLives, 1000), new CounterSet(0, remainingLives * 1000, 500));
			ballsCounter.setTimingFunction(Easing.easeOutSine);

			FormattedCounterEffect totalCounterThree = new FormattedCounterEffect("Total Score: %d", winTotalScoreField, new CounterSet(score, score + bonusScore + remainingLives * 1000, 1000));
			totalCounterThree.setTimingFunction(Easing.easeOutSine);

			CompoundEffect stepThree = new CompoundEffect(true, ballsCounter, totalCounterThree);

			stepTwo.queueEffect(stepThree);

			winScreenEffects.add(stepThree);
		return EventPropagation.CONTINUE;
	}

	public int getRemainingLives() {
		return remainingLives;
	}

	public float getScoreMultiplier() {
		return scoreMultiplier;
	}

	@Override
	public void inputEnded() {

	}

	@Override
	public void inputStarted() {

	}

	@Override
	public boolean isAcceptingInput() {
		return true;
	}

	public boolean isPaused() {
		return currentState != GameState.PLAYING;
	}

	@Override
	public void keyPressed(int key, char c) {
	}

	@Override
	public void keyReleased(int key, char c) {
		switch (currentState) {
			case PAUSED:
				if (key == Input.KEY_ESCAPE) {
					currentState = GameState.PLAYING;
				}
				break;
			case PLAYING:
				if (key == Input.KEY_ESCAPE) {
					currentState = GameState.PAUSED;
				}
				break;
			case GAMEOVER:
				break;
			case PRE:
				break;
			default:
				break;

		}
	}

	@Override
	public void mouseClicked(int button, int x, int y, int clickCount) {

	}

	@Override
	public void mouseDragged(int oldx, int oldy, int newx, int newy) {

	}

	@Override
	public void mouseMoved(int oldx, int oldy, int newx, int newy) {

	}

	@Override
	public void mousePressed(int button, int x, int y) {

	}

	@Override
	public void mouseReleased(int button, int x, int y) {

	}

	@Override
	public void mouseWheelMoved(int change) {

	}

	public void movePaddle(float delta, EntityPaddle paddle) {
		delta = ((paddle.getPosition().x + delta) < 0 ? -paddle.getPosition().x : delta);
		delta = ((paddle.getPosition().x + delta) > (GameInfo.GAME_WIDTH - paddle.getBoundingBox().getWidth()) ? (GameInfo.GAME_WIDTH - paddle.getBoundingBox().getWidth()) - paddle.getPosition().x : delta);
		paddle.setPosition(paddle.getPosition().add(new Vector2f(delta, 0)));
	}

	public void powerupBecameActive(Powerup powerup) {
		activePowerups.add(powerup);
	}

	@EventListener
	public EventPropagation powerupHitPaddleEvent(PowerupDidHitPaddleEvent event) {
		EntityPowerup entityPowerup = event.getPowerup();

		entityPowerup.getCurrentPowerup().activate();
		activePowerups.add(entityPowerup.getCurrentPowerup());

		return EventPropagation.CONTINUE;
	}

	public void powerupWasDeactivated(EntityPowerup entityPowerup) {
		activePowerups.remove(entityPowerup);
	}

	@EventListener
	public EventPropagation powerupWillHitPaddleEvent(PowerupWillHitPaddleEvent event) {
		if (event.getPowerup().isDead()) { return EventPropagation.STOP; }

		return EventPropagation.CONTINUE;

	}

	public void removeGameSpeed(float sub) {
		this.gameSpeed -= sub;
	}

	public void removeScoreMultipler(float f) {
		scoreMultiplier -= f;
	}

	public void revertPaddle(EntityPaddle paddle) {
		if (paddle == paddle1) {
			paddleSwapCounter1--;

			if (paddleSwapCounter1 == 0) {
				paddle1.setDead();

				try {
					EntityPaddle newPaddle = BallisticBees.getPaddleRegistry().getPaddle("vanilla:paddle_normal");
					newPaddle.setPosition(paddle1.getPosition());
					paddle1 = newPaddle;
					world.registerEntity(paddle1, PhysicsLayer.LAYER_1);
					player1.setPaddle(paddle1);
				} catch (BallisticBeesException e2) {
					e2.printStackTrace();
				}

			}
		}

		if (multiplayer && paddle == paddle2) {
			paddleSwapCounter2--;

			if (paddleSwapCounter2 == 0) {
				paddle2.setDead();

				try {
					EntityPaddle newPaddle = BallisticBees.getPaddleRegistry().getPaddle("vanilla:paddle_normal");
					newPaddle.setPosition(paddle2.getPosition());
					paddle2 = newPaddle;
					world.registerEntity(paddle2, PhysicsLayer.LAYER_1);
					player2.setPaddle(paddle2);
				} catch (BallisticBeesException e2) {
					e2.printStackTrace();
				}

			}
		}
	}

	@Override
	public void setInput(Input arg0) {

	}

	private void setupLevels() {
		try {
			for (String levelId : gameDef.getLevels()) {
				Level level = BallisticBees.getLevelRegistry().getLevel(levelId);
				level.setWorld(world);
				levels.add(level);
			}
		} catch (BallisticBeesException e) {
			e.printStackTrace();
		}
	}

	private void showNextLevelText(int levelNum) {
		TextField levelName = new TextField(container, 0, "Level " + levelNum, GameInfo.GAME_WIDTH / 2 - 100, 200, 300, 100, MainFont.font40, Color.yellow);

		FadeEffect fade = new FadeEffect(levelName, 1, 0, 1000);

		currentEffects.add(fade);
	}

	public void swapPaddle(EntityPaddle newPaddle, EntityPaddle oldPaddle) {
		if (oldPaddle == paddle1) {
			paddle1.setDead();
			newPaddle.setPosition(paddle1.getPosition());

			paddle1 = newPaddle;

			world.registerEntity(paddle1, PhysicsLayer.LAYER_1);

			paddleSwapCounter1++;

			player1.setPaddle(paddle1);
		}

		if (multiplayer && oldPaddle == paddle2) {
			paddle2.setDead();
			newPaddle.setPosition(paddle2.getPosition());

			paddle2 = newPaddle;

			world.registerEntity(paddle2, PhysicsLayer.LAYER_1);

			paddleSwapCounter2++;

			player2.setPaddle(paddle2);
		}
	}

	@Override
	public void tick(float delta) {
		delta *= gameSpeed;

		tickAlways(delta);

		switch (currentState) {
			case PAUSED:
				backToMenuButton.setAcceptingInput(true);
				break;
			case PLAYING:
				tickPlaying(delta);
				break;
			case GAMEOVER:
				tickEnd(delta);
				backToMenuButton.setAcceptingInput(true);
				break;
			case PRE:
				currentState = GameState.PLAYING;
				break;
			case WIN:
				tickEnd(delta);
				backToMenuButton.setAcceptingInput(true);
				break;
			default:
				break;
		}
	}

	public void tickAlways(float delta) {
		switch (player1.getDirective()) {
			case LEFT:
				movePaddle(-1 * BallisticBees.physicsSpeed, paddle1);
				break;
			case RIGHT:
				movePaddle(1 * BallisticBees.physicsSpeed, paddle1);
				break;
			default:
				break;
		}

		if (player2 != null) {
			switch (player2.getDirective()) {
				case LEFT:
					movePaddle(-1 * BallisticBees.physicsSpeed, paddle2);
					break;
				case RIGHT:
					movePaddle(1 * BallisticBees.physicsSpeed, paddle2);
					break;
				default:
					break;
			}
		}

		for (Iterator<Effect> iterator = currentEffects.iterator(); iterator.hasNext();) {
			Effect effect = iterator.next();
			if (effect.isFinished(true)) {
				iterator.remove();
				continue;
			}
			effect.tick(delta);
		}

		for (Iterator<Entity> iterator = entities.iterator(); iterator.hasNext();) {
			Entity e = iterator.next();
			if (e.isDead()) iterator.remove();
		}

		if (currentLevel.isComplete() && levels.size() > 0) {
			// Reset all objects from physics
			world.clearLayer(PhysicsLayer.LAYER_1);
			// re-add the ball
			try {
				world.registerEntity(paddle1, PhysicsLayer.LAYER_1);
				if (multiplayer) {
					world.registerEntity(paddle2, PhysicsLayer.LAYER_1);
				}
			} catch (BallisticBeesException e1) {
				e1.printStackTrace();
			}

			currentLevel = levels.get(0);
			currentLevel.initRows();
			levels.remove(0);
			showNextLevelText(levelCounter++);

			clearEntitiesOnLevelEnd();

			player1.setState(PlayerState.Waiting);
			player1.setLoadedBall(addBall(true, paddle1));

			if (multiplayer) {
				player2.setState(PlayerState.Waiting);
				player2.setLoadedBall(addBall(true, paddle2));
			}
		}
	}

	public void tickPlaying(float delta) {
		world.step(delta);

		currentLevel.tick(delta);

		for (Iterator<Powerup> iterator = activePowerups.iterator(); iterator.hasNext();) {
			Powerup p = iterator.next();
			p.tick(delta);

			if (p.isComplete()) {
				p.deactivate();
				iterator.remove();
			}
		}

		player1.tick(delta);

		if (multiplayer) {
			player2.tick(delta);
		}
	}

	public void tickEnd(float delta) {
		for (Iterator<Effect> iterator = winScreenEffects.iterator(); iterator.hasNext();) {
			Effect effect = iterator.next();
			effect.tick(delta);
		}
	}

}
