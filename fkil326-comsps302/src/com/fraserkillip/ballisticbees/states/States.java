package com.fraserkillip.ballisticbees.states;

import org.newdawn.slick.state.GameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 * This class stores static references to the states, and initialises them
 * 
 * @author fraserkillip
 *
 */
public class States {
	
	// Static references to the states
	public static GameState STATE_GAME_SELECT;
	public static GameState STATE_HELP;
	public static GameState STATE_LOADING;
	public static GameState STATE_MENU;
	public static GameState STATE_NAME_INPUT;
	public static GameState STATE_PLAYING;
	public static GameState STATE_SETTINGS;
	public static GameState STATE_SPLASH;
	
	// Initialise the states
	public static void initStates(StateBasedGame game) {
		int id = 0;
		STATE_GAME_SELECT = new StateGameSelect(id++);
		STATE_HELP = new StateHelp(id++);
		STATE_LOADING = new StateLoading(id++);
		STATE_MENU = new StateMenu(id++);
		STATE_NAME_INPUT = new StateNameInput(id++);
		STATE_PLAYING = new StatePlaying(id++);
		STATE_SETTINGS = new StateSettings(id++);
		STATE_SPLASH = new StateSplash(id++);

		game.addState(STATE_SPLASH);
		game.addState(STATE_GAME_SELECT);
		game.addState(STATE_HELP);
		game.addState(STATE_LOADING);
		game.addState(STATE_MENU);
		game.addState(STATE_NAME_INPUT);
		game.addState(STATE_PLAYING);
		game.addState(STATE_SETTINGS);
	}
}
