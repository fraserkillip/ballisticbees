package com.fraserkillip.ballisticbees.states;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.fraserkillip.ballisticbees.graphics.render.Background;

/**
 * Defines a few common elements between states
 * 
 * 
 * @author fraserkillip
 *
 */
public abstract class BaseState extends BasicGameState {
	private int ID;
	
	protected StateBasedGame stateBasedGame;

	public BaseState(int ID) {
		this.ID = ID;
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return ID;
	}
	
	@Override
	public void update(GameContainer arg0, StateBasedGame arg1, int delta) throws SlickException {
		Background.getInstance().tick(delta);
	}
	
	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		stateBasedGame = game;
	}
}
