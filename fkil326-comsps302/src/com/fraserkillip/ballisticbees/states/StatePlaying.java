package com.fraserkillip.ballisticbees.states;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.fraserkillip.ballisticbees.game.Game;
import com.fraserkillip.ballisticbees.graphics.render.Background;
import com.fraserkillip.ballisticbees.pack.definitions.GameDefinition;

/**
 * Contains logic to manage the current playing game
 * 
 * @author fraserkillip
 *
 */
public class StatePlaying extends BaseState {
	
	// Store the game and the container for later use
	private Game currentGame;
	
	private GameDefinition gameDef;
	
	private boolean multiplayer = false;
	
	public StatePlaying(int ID) {
		super(ID);
	}

	@Override
	public void init(GameContainer arg0, StateBasedGame arg1) throws SlickException {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException {
		// TODO Auto-generated method stub
		super.enter(container, game);
		if(gameDef == null) {
			game.enterState(States.STATE_GAME_SELECT.getID());
		}
		System.out.println(multiplayer);
		currentGame = new Game(container, game, gameDef, multiplayer);
	}
	
	public boolean isMultiplayer() {
		return multiplayer;
	}

	public void setMultiplayer(boolean multiplayer) {
		this.multiplayer = multiplayer;
	}

	public Game getGame() {
		return currentGame;
	}

	// Propagate the following events to the game
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		Background.getInstance().draw(0, 0, g);

		currentGame.draw(100, 100, g);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		super.update(container, game, delta);
		currentGame.tick(delta);
	}
	
	@Override
	public void mouseClicked(int arg0, int arg1, int arg2, int arg3) {
		currentGame.mouseClicked(arg0, arg1, arg2, arg3);
	}

	@Override
	public void mouseDragged(int arg0, int arg1, int arg2, int arg3) {
		currentGame.mouseDragged(arg0, arg1, arg2, arg3);
	}

	@Override
	public void mouseMoved(int oldx, int oldy, int newx, int newy) {
		currentGame.mouseMoved(oldx, oldy, newx, newy);
	}

	@Override
	public void mousePressed(int arg0, int arg1, int arg2) {
		currentGame.mousePressed(arg0, arg1, arg2);
	}

	@Override
	public void mouseReleased(int arg0, int arg1, int arg2) {
		currentGame.mouseReleased(arg0, arg1, arg2);
	}

	@Override
	public void mouseWheelMoved(int arg0) {
		currentGame.mouseWheelMoved(arg0);
	}

	@Override
	public void keyPressed(int arg0, char arg1) {
		currentGame.keyPressed(arg0, arg1);
	}

	@Override
	public void keyReleased(int arg0, char arg1) {
		currentGame.keyReleased(arg0, arg1);
	}
	// END: Propagated events

	public GameDefinition getGameDef() {
		return gameDef;
	}

	public void setGameDef(GameDefinition gameDef) {
		this.gameDef = gameDef;
	}
}
