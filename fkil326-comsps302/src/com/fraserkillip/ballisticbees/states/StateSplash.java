package com.fraserkillip.ballisticbees.states;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.loading.LoadingList;
import org.newdawn.slick.state.StateBasedGame;

import com.fraserkillip.ballisticbees.BallisticBees;
import com.fraserkillip.ballisticbees.exceptions.BallisticBeesException;
import com.fraserkillip.ballisticbees.exceptions.PackNotLoadedException;
import com.fraserkillip.ballisticbees.graphics.font.MainFont;
import com.fraserkillip.ballisticbees.graphics.render.Images;
import com.fraserkillip.ballisticbees.pack.PackLoader;

/**
 * This state is just pretty and makes loading seem cool
 * 
 * @author fraserkillip
 * 
 */
public class StateSplash extends BaseState {
	private int rendered = 100;

	public StateSplash(int ID) {
		super(ID);
	}

	@Override
	public void enter(GameContainer arg0, StateBasedGame arg1) throws SlickException {
	}

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
	}

	@Override
	public void leave(GameContainer arg0, StateBasedGame arg1) throws SlickException {
		// TODO Auto-generated method stub
		MainFont.initFonts();
	}

	// Render single image to the middle of the screen
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		g.setBackground(Color.white);

		g.setAntiAlias(true);

		Images.logo.drawCentered(400, 300);

		g.setAntiAlias(false);

		rendered--;
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		// Let 100 frames render so we don't just see black
		if (rendered == 0) {
			LoadingList.setDeferredLoading(true);
			System.out.println("Loading packs");
			PackLoader.loadPacks();

			System.out.println("Initialising packs");
			try {
				BallisticBees.getPackRegistry().initPacks();
			} catch (PackNotLoadedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (BallisticBeesException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			game.enterState(States.STATE_LOADING.getID());
		}
	}
}
