package com.fraserkillip.ballisticbees.states;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.fraserkillip.ballisticbees.BallisticBees;
import com.fraserkillip.ballisticbees.GameInfo;
import com.fraserkillip.ballisticbees.graphics.gui.Button;
import com.fraserkillip.ballisticbees.graphics.gui.ButtonListener;
import com.fraserkillip.ballisticbees.graphics.gui.GameDisplay;
import com.fraserkillip.ballisticbees.graphics.gui.RadioGroup;
import com.fraserkillip.ballisticbees.graphics.gui.ScrollingContainer;
import com.fraserkillip.ballisticbees.graphics.render.Background;
import com.fraserkillip.ballisticbees.pack.definitions.GameDefinition;

public class StateGameSelect extends BaseState implements ButtonListener {

	private Button playButton = null, menuButton = null;
	
	private ScrollingContainer scrollContainer = null;
	
	private RadioGroup group;
	
	public StateGameSelect(int ID) {
		super(ID);
	}
	
	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException {
		super.enter(container, game);
		scrollContainer = new ScrollingContainer(container, 0, 50, 100, 700, 400, BallisticBees.getGameRegistry().getAllGames().size()*220 + 20);
		
		playButton = new Button(container, 0, "PLAY!", GameInfo.GAME_WIDTH/2 - 250, 525, 200, 50);
		playButton.setListener(this);
		
		menuButton = new Button(container, 1, "Main Menu", GameInfo.GAME_WIDTH/2 + 50, 525, 200, 50);
		menuButton.setListener(this);
		
		group = new RadioGroup();
		
		int panelNum = 0;
		for(GameDefinition gameDef : BallisticBees.getGameRegistry().getAllGames()) {
			GameDisplay gameDisp = new GameDisplay(container, panelNum, panelNum,gameDef, group.createButton(container, panelNum, "select", 0, 0, 0, 0));
			scrollContainer.addComponent(gameDisp);
			panelNum++;
		}
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		Background.getInstance().draw(0, 0, g);

		scrollContainer.draw(0, 0, g);
		
		if(playButton != null) playButton.draw(0, 0, g);
		if(menuButton != null) menuButton.draw(0, 0, g);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		super.update(container, game, delta);
	}
	
	@Override
	public void leave(GameContainer container, StateBasedGame game) throws SlickException {
		playButton = playButton.dispose();
		menuButton = menuButton.dispose();
		group = group.dispose();
		scrollContainer = null;
	};

	@Override
	public void buttonPressed(int id) {
		switch (id) {
			case 0:
				int checked = group.getCheckedId();
				if(checked != -1){
					((StatePlaying) States.STATE_PLAYING).setGameDef(BallisticBees.getGameRegistry().getAllGames().get(checked));
					((StatePlaying) States.STATE_PLAYING).setMultiplayer(((GameDisplay) scrollContainer.getComponent(checked)).getMultiplayer());
					stateBasedGame.enterState(States.STATE_PLAYING.getID());
				}
				break;
			case 1:
				stateBasedGame.enterState(States.STATE_MENU.getID());
				break;
		}
	}

}
