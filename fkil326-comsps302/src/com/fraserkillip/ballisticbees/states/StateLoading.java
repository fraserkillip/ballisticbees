package com.fraserkillip.ballisticbees.states;

import java.io.IOException;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.loading.DeferredResource;
import org.newdawn.slick.loading.LoadingList;
import org.newdawn.slick.state.StateBasedGame;

import com.fraserkillip.ballisticbees.GameInfo;
import com.fraserkillip.ballisticbees.graphics.font.MainFont;
import com.fraserkillip.ballisticbees.graphics.gui.ProgressBar;
import com.fraserkillip.ballisticbees.graphics.render.Background;

/**
 * This state loads in all predefined resources
 * 
 * Currently this happens to fast to notice
 * 
 * @author fraserkillip
 *
 */
public class StateLoading extends BaseState {

	private final String loadingString = "Loading...";
	
	private ProgressBar progressBar;

	public StateLoading(int ID) {
		super(ID);
	}

	@Override
	public void init(GameContainer arg0, StateBasedGame arg1) throws SlickException {
	}

	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException {
		// Init progress bar
		progressBar = new ProgressBar(300, 0);
	};

	// Draw the screen
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		Background.getInstance().draw(0, 0, g);

		g.setColor(Color.black);
		g.setFont(MainFont.font40);

		g.drawString(loadingString, 400 - (g.getFont().getWidth(loadingString) / 2), 300 - (g.getFont().getHeight(loadingString) / 2));
		
		progressBar.draw(GameInfo.GAME_WIDTH/2 - progressBar.getWidth()/2, 500, g);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		super.update(container, game, delta);
		
		// If we have resources left, load one
		/*if (LoadingList.get().getRemainingResources() > 0) {
			// Get the next resource
			LoadingList list = LoadingList.get();
			// Update the progress bar
			progressBar.setProgress((float)(list.getTotalResources() - list.getRemainingResources()) / (float)list.getTotalResources());
			try {
				// Load the next resource
				DeferredResource next = list.getNext();
				System.out.println(next.getDescription());
				next.load();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {*/
			// Transition to the menu
			game.enterState(States.STATE_MENU.getID());
//		}
	}
}
