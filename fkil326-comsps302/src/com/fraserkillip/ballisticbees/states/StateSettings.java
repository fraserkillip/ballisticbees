package com.fraserkillip.ballisticbees.states;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.fraserkillip.ballisticbees.BallisticBees;
import com.fraserkillip.ballisticbees.GameInfo;
import com.fraserkillip.ballisticbees.graphics.font.MainFont;
import com.fraserkillip.ballisticbees.graphics.gui.Button;
import com.fraserkillip.ballisticbees.graphics.gui.ButtonListener;
import com.fraserkillip.ballisticbees.graphics.gui.RadioButton;
import com.fraserkillip.ballisticbees.graphics.gui.RadioGroup;
import com.fraserkillip.ballisticbees.graphics.gui.Slider;
import com.fraserkillip.ballisticbees.graphics.gui.SliderListener;
import com.fraserkillip.ballisticbees.graphics.render.Background;
import com.fraserkillip.ballisticbees.player.Player;

/**
 * State to control user settings
 * 
 * @author fraserkillip
 * 
 */

//TODO: Class far from complete.
public class StateSettings extends BaseState implements SliderListener, ButtonListener {

	// Declartion of buttons and other UI widgets
	private Button menuButton;

	private RadioGroup playerSelect;

	private Button leftButton, rightButton, fireButton;

	private Player selectedPlayer;

	private boolean waitingForKey = false;

	private Slider musicVolumeSlider, soundVolumeSlider;

	private GameContainer container;

	public StateSettings(int ID) {
		super(ID);
	}

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		super.init(container, game);

		this.container = container;

		container.setMusicVolume(BallisticBees.getSettings().musicVolume);
		container.setSoundVolume(BallisticBees.getSettings().soundVolume);
	}

	// Set up widgets
	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException {
		super.enter(container, game);

		menuButton = new Button(container, 0, "MAIN MENU", 300, GameInfo.GAME_HEIGHT - 100, 200, 75);

		menuButton.setListener(this);

		musicVolumeSlider = new Slider(container, 0, 420, 100, 200, 30, 100, 0);
		musicVolumeSlider.setValue((int) (BallisticBees.getSettings().musicVolume * 100));
		musicVolumeSlider.setListener(this);

		soundVolumeSlider = new Slider(container, 1, 420, 150, 200, 30, 100, 0);
		soundVolumeSlider.setValue((int) (BallisticBees.getSettings().soundVolume * 100));
		soundVolumeSlider.setListener(this);

		playerSelect = new RadioGroup();

		RadioButton player1 = playerSelect.createButton(container, 1, "Player 1", GameInfo.GAME_WIDTH / 2 - 170, GameInfo.GAME_HEIGHT / 2 - 80, 120, 50);
		RadioButton player2 = playerSelect.createButton(container, 2, "Player 2", GameInfo.GAME_WIDTH / 2 + 50, GameInfo.GAME_HEIGHT / 2 - 80, 120, 50);
		player1.setListener(this);
		player2.setListener(this);

		playerSelect.check(player1);
		selectedPlayer = Player.Player1;

		System.out.println(BallisticBees.getSettings().Player1_left);
		leftButton = new Button(container, 3, Input.getKeyName(BallisticBees.getSettings().Player1_left), GameInfo.GAME_WIDTH / 2 + 20, GameInfo.GAME_HEIGHT / 2, 200, 40);
		rightButton = new Button(container, 4, Input.getKeyName(BallisticBees.getSettings().Player1_right), GameInfo.GAME_WIDTH / 2 + 20, GameInfo.GAME_HEIGHT / 2 + 60, 200, 40);
		fireButton = new Button(container, 5, Input.getKeyName(BallisticBees.getSettings().Player1_fire), GameInfo.GAME_WIDTH / 2 + 20, GameInfo.GAME_HEIGHT / 2 + 120, 200, 40);

		leftButton.setListener(this);
		rightButton.setListener(this);
		fireButton.setListener(this);
	}

	@Override
	public void leave(GameContainer container, StateBasedGame game) throws SlickException {
		super.leave(container, game);

		menuButton = menuButton.dispose();

		playerSelect = playerSelect.dispose();

		soundVolumeSlider.dispose();
		musicVolumeSlider.dispose();

		leftButton.dispose();
		rightButton.dispose();
		fireButton.dispose();
	}

	// Draw all to screen
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		Background.getInstance().draw(0, 0, g);

		g.setFont(MainFont.font20);
		g.drawString("Music Volume:", 380 - MainFont.font20.getWidth("MUSIC VOLUME:"), 100);
		musicVolumeSlider.draw(0, 0, g);

		g.drawString("Sound Volume:", 380 - MainFont.font20.getWidth("SOUND VOLUME:"), 150);
		soundVolumeSlider.draw(0, 0, g);

		playerSelect.draw(0, 0, g);

		g.drawString("Left Key:", 410 - MainFont.font20.getWidth("LEFT KEY:"), GameInfo.GAME_HEIGHT / 2 + 5);
		leftButton.draw(0, 0, g);

		g.drawString("Right Key:", 410 - MainFont.font20.getWidth("RIGHT KEY:"), GameInfo.GAME_HEIGHT / 2 + 65);
		rightButton.draw(0, 0, g);

		g.drawString("Fire Key:", 410 - MainFont.font20.getWidth("FIRE KEY:"), GameInfo.GAME_HEIGHT / 2 + 125);
		fireButton.draw(0, 0, g);

		menuButton.draw(0, 0, g);

	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		super.update(container, game, delta);
	}

	@Override
	public void sliderValueUpdated(int id) {
		switch (id) {
			case 0:
				container.setMusicVolume(musicVolumeSlider.getValue() / 100.0f);
				BallisticBees.getSettings().musicVolume = musicVolumeSlider.getValue() / 100.0f;
				break;
			case 1:
				container.setSoundVolume(soundVolumeSlider.getValue() / 100.0f);
				BallisticBees.getSettings().soundVolume = soundVolumeSlider.getValue() / 100.0f;
				break;
		}
		BallisticBees.saveSettings();
	}

	@Override
	public void buttonPressed(int id) {
		switch (id) {
			case 0:
				stateBasedGame.enterState(States.STATE_MENU.getID());
				break;
			case 1:
				selectedPlayer = Player.Player1;
				waitingForKey = false;
				resetKeyButtons();
				break;
			case 2:
				selectedPlayer = Player.Player2;
				waitingForKey = false;
				resetKeyButtons();
				break;
			case 3: // Left Button
				resetKeyButtons();
				leftButton.setMessage("Press a key...");
				waitingForKey = true;
				break;
			case 4: // Right Button
				resetKeyButtons();
				rightButton.setMessage("Press a key...");
				waitingForKey = true;
				break;
			case 5: // Fire Button
				resetKeyButtons();
				fireButton.setMessage("Press a key...");
				waitingForKey = true;
				break;
		}
	}

	private void resetKeyButtons() {
		switch (selectedPlayer) {
		// Player1 is selected
			case Player1:
				leftButton.setMessage(Input.getKeyName(BallisticBees.getSettings().Player1_left));
				rightButton.setMessage(Input.getKeyName(BallisticBees.getSettings().Player1_right));
				fireButton.setMessage(Input.getKeyName(BallisticBees.getSettings().Player1_fire));
				break;
			case Player2:
				leftButton.setMessage(Input.getKeyName(BallisticBees.getSettings().Player2_left));
				rightButton.setMessage(Input.getKeyName(BallisticBees.getSettings().Player2_right));
				fireButton.setMessage(Input.getKeyName(BallisticBees.getSettings().Player2_fire));
				break;
			default:
				break;
		}
	}

	@Override
	public void keyPressed(int key, char c) {
		if (key == Input.KEY_ESCAPE) {
			resetKeyButtons();
			waitingForKey = false;
			return;
		}

		if (leftButton.getMessage() == "Press a key...") {
			if (selectedPlayer == Player.Player1) {
				BallisticBees.getSettings().Player1_left = key;
			} else {
				BallisticBees.getSettings().Player2_left = key;
			}
		} else if (rightButton.getMessage() == "Press a key...") {
			if (selectedPlayer == Player.Player1) {
				BallisticBees.getSettings().Player1_right = key;
			} else {
				BallisticBees.getSettings().Player2_right = key;
			}
		} else if (fireButton.getMessage() == "Press a key...") {
			if (selectedPlayer == Player.Player1) {
				BallisticBees.getSettings().Player1_fire = key;
			} else {
				BallisticBees.getSettings().Player2_fire = key;
			}
		}
		
		BallisticBees.saveSettings();

		resetKeyButtons();
		waitingForKey = false;
	}
}
