package com.fraserkillip.ballisticbees.states;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import com.fraserkillip.ballisticbees.GameInfo;
import com.fraserkillip.ballisticbees.audio.GameMusic;
import com.fraserkillip.ballisticbees.graphics.font.MainFont;
import com.fraserkillip.ballisticbees.graphics.fx.PulsingEffect;
import com.fraserkillip.ballisticbees.graphics.gui.Button;
import com.fraserkillip.ballisticbees.graphics.gui.ButtonListener;
import com.fraserkillip.ballisticbees.graphics.gui.TextField;
import com.fraserkillip.ballisticbees.graphics.render.Background;

/**
 * Menu State
 * 
 * @author fraserkillip
 *
 */
public class StateMenu extends BaseState implements ButtonListener {

	// Store the buttons for later access
	public Button singlePlayerButton;
	public Button quitButton;
	public Button settingsButton;
	
	// The title TextField and scale pulser
	private TextField title;	
	private PulsingEffect titlePulser;
	
	private GameContainer container;
	
	// Constructor
	public StateMenu(int id) {
		super(id);
	}

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		super.init(container, game);
		this.container = container;
	}

	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException {
		super.enter(container, game);
		
		// When we enter the state set up our buttons and text
		singlePlayerButton = new Button(container, 0, "PLAY!", 300, 200, 200, 75);
		quitButton = new Button(container, 1, "QUIT", 300, 400, 200, 75);
		settingsButton = new Button(container, 2, "SETTINGS", 300, 300, 200, 75);
		
		singlePlayerButton.setListener(this);
		quitButton.setListener(this);
		settingsButton.setListener(this);
	
		title = new TextField(container, 0, GameInfo.GAME_TITLE, GameInfo.GAME_WIDTH/2 - MainFont.font40.getWidth(GameInfo.GAME_TITLE)/2, 75, 0, 0, MainFont.font40, Color.black);
		
		titlePulser = new PulsingEffect(title, 1.0F, 1.2F, 1500F, -1, new Vector2f(GameInfo.GAME_WIDTH/2, GameInfo.GAME_HEIGHT / 2 - 200));
		titlePulser.start();
		
		if(!GameMusic.theme.playing()) GameMusic.theme.loop();
	}
	
	@Override
	public void leave(GameContainer container, StateBasedGame game) throws SlickException {
		super.leave(container, game);
		System.out.println("Leaving");
		singlePlayerButton = singlePlayerButton.dispose();
		quitButton = quitButton.dispose();
		settingsButton = settingsButton.dispose();
	}

	// Draw all elements
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		Background.getInstance().draw(0, 0, g);
		
		singlePlayerButton.draw(0, 0, g);
		quitButton.draw(0, 0, g);
		settingsButton.draw(0, 0, g);
		
		titlePulser.draw(0, 0, g);
	}
	
	// Perform Updates
	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		super.update(container, game, delta);
		
		titlePulser.tick(delta);
	}

	@Override
	public void buttonPressed(int id) {
		switch (id) {
			case 0:
				stateBasedGame.enterState(States.STATE_GAME_SELECT.getID());
			break;
			case 1:
				container.exit();
				break;
			case 2:
				stateBasedGame.enterState(States.STATE_SETTINGS.getID());
				break;
		}
	}
}
