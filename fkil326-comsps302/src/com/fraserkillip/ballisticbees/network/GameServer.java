package com.fraserkillip.ballisticbees.network;

import java.io.IOException;
import java.net.DatagramSocket;

/**
 * Handles network communications for the game
 * 
 * Unused
 * 
 * @author fraserkillip
 *
 */
public class GameServer extends Thread {

	protected DatagramSocket socket = null;

	public GameServer() throws IOException {
		this("QuoteServerThread");
	}

	public GameServer(String name) throws IOException {
		super(name);
		socket = new DatagramSocket(4445);
	}

}
