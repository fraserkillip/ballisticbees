package com.fraserkillip.ballisticbees.network;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.DatagramPacket;

import com.fraserkillip.ballisticbees.player.PlayerDirective;

/**
 * Handles all incoming packets from the network
 * 
 * Class not used ATM
 * 
 * @author fraserkillip
 *
 */
public class PacketHandler {

	public static void OnPacketReceived(DatagramPacket packet) {
		byte[] data = packet.getData();

		ByteArrayInputStream byteStream = new ByteArrayInputStream(data);
		DataInputStream dataStream = new DataInputStream(byteStream);

		byte packetId = -1;
		try {
			packetId = dataStream.readByte();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (packetId == -1) {
			System.err.println("Received invalid packet.");
			return;
		}

		switch (packetId) {
		default:
			System.err.println("Received unhandled packet with id: " + packetId);
		}
	}

	public static void sendPlayerControlUpdate(PlayerDirective control, boolean state) {
		// TODO Send packet data
	}

	public static void sendPlayerNameUpdate(String name) {
		// TODO Send packet data
	}
}
