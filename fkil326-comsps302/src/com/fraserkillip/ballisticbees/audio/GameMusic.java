package com.fraserkillip.ballisticbees.audio;

import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.loading.LoadingList;

public class GameMusic {
	
	public static Music theme;
	
	public static void initMusic() throws SlickException{
		LoadingList.setDeferredLoading(false);
		theme = new Music("assets/music/theme.ogg");
	}

}
