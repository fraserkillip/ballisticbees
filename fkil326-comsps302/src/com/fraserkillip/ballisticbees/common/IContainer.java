package com.fraserkillip.ballisticbees.common;

public interface IContainer {

	public int getxPos();

	public int getyPos();

}
