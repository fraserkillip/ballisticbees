package com.fraserkillip.ballisticbees.common;

public interface ITickable {

	public abstract void tick(float delta);

}