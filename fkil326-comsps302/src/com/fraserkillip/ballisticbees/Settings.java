package com.fraserkillip.ballisticbees;

import java.io.Serializable;

import org.newdawn.slick.Input;

/**
 * Used to persist use settings
 * 
 * 
 * Not yet implemented
 * 
 * @author fraserkillip
 *
 */
public class Settings implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public float soundVolume = 1.0f;
	
	public float musicVolume = 1.0f;

	public int Player1_left = Input.KEY_A, Player1_right = Input.KEY_D, Player1_fire = Input.KEY_SPACE;

	public int Player2_left = Input.KEY_LEFT, Player2_right = Input.KEY_RIGHT, Player2_fire = Input.KEY_ENTER;

}
