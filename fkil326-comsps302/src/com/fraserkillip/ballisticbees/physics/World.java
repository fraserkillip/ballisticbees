package com.fraserkillip.ballisticbees.physics;

import java.util.ArrayList;
import java.util.Iterator;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;

import com.fraserkillip.ballisticbees.BallisticBees;
import com.fraserkillip.ballisticbees.GameInfo;
import com.fraserkillip.ballisticbees.common.ITickable;
import com.fraserkillip.ballisticbees.entity.CollidableEntity;
import com.fraserkillip.ballisticbees.entity.Entity;
import com.fraserkillip.ballisticbees.entity.ball.EntityBall;
import com.fraserkillip.ballisticbees.entity.brick.EntityBrick;
import com.fraserkillip.ballisticbees.entity.paddle.EntityPaddle;
import com.fraserkillip.ballisticbees.entity.powerup.EntityPowerup;
import com.fraserkillip.ballisticbees.event.EventEngine;
import com.fraserkillip.ballisticbees.exceptions.AlreadyInWorldException;
import com.fraserkillip.ballisticbees.exceptions.BallisticBeesException;
import com.fraserkillip.ballisticbees.exceptions.ObjectNotCollidableException;
import com.fraserkillip.ballisticbees.game.powerup.AIPowerup;
import com.fraserkillip.ballisticbees.game.powerup.ExtraBallPowerup;
import com.fraserkillip.ballisticbees.game.powerup.LargerPaddlePowerup;
import com.fraserkillip.ballisticbees.game.powerup.Powerup;
import com.fraserkillip.ballisticbees.graphics.IRenderable;

/**
 * Manages and runs all physics
 * 
 * Phsyics will be able to be run on multiple layers with seperate collision
 * detection, currently only layer 1 and non-collision is implemented
 * 
 * @author fraserkillip
 * 
 */
public class World implements IRenderable {

	public enum PhysicsLayer {
		NO_COLLISION, LAYER_1, LAYER_2, LAYER_3
	}

	// Used to store an easy to access reference to all entities in the world
	private ArrayList<Entity> allEntities;

	// Used for objects that require physics but do not collide (EG Clouds)
	private ArrayList<Entity> layerNoCollision;

	// Provides separate collision layers. EG multiplayer games
	private ArrayList<CollidableEntity> layer1;
	private ArrayList<CollidableEntity> layer2;
	private ArrayList<CollidableEntity> layer3;

	// Boundaries of the world
	private ArrayList<Boundary> boundaries;

	// Debug properties
	private Vector2f lastNormal = null;
	private Vector2f lastTangent = null;
	private Vector2f lastCollisionPoint = null;
	private Line lastCollisionLine = null;
	private float lastUps = 0;
	private long lastUpsTime = 0;
	private int currentUpdates = 0;

	// Constructor
	public World() {
		// Init variable
		allEntities = new ArrayList<Entity>();

		layerNoCollision = new ArrayList<Entity>();

		layer1 = new ArrayList<CollidableEntity>();
		layer2 = new ArrayList<CollidableEntity>();
		layer3 = new ArrayList<CollidableEntity>();

		boundaries = new ArrayList<Boundary>();
	}

	// Step the physics by *delta* milliseconds
	public void step(float delta) {
		delta *= BallisticBees.physicsSpeed; // Speed up physics by defined constant
		// Calculate physics tps
		if (lastUpsTime < System.currentTimeMillis() - 200) {
			lastUps = currentUpdates / 0.2f;
			lastUpsTime = System.currentTimeMillis();
			currentUpdates = 0;
		}
		currentUpdates++;

		// Update all entities positions and remove dead ones
		for (Iterator<Entity> iterator = allEntities.iterator(); iterator.hasNext();) {
			Entity entity = (Entity) iterator.next();

			entity.tick(delta);

			if (entity.isDead()) {
				iterator.remove();

				layer1.remove(entity);
				layer2.remove(entity);
				layer3.remove(entity);
				layerNoCollision.remove(entity);
			}
		}

		// For every entity in layer one, check collisions with the other
		// entities in the layer
		int numberOfEntities = layer1.size();
		for (int i = 0; i < numberOfEntities; i++) {
			CollidableEntity base = layer1.get(i);
			// If the base is not currently active for collisions skip it
			if (!base.getActive()) continue;

			// We assume that the base is a circle in the calculations so skip
			// if it is not
			if (!(base.getBoundingBox() instanceof Circle)) continue;

			// Collide with boundaries
			for (Boundary boundary : boundaries) {
				if (base.doesIntersect(boundary.getBoundingBox())) {
					doCollision(base, boundary, delta);
				}
			}

			// Loop all enties again and collide with them
			for (int j = 0; j < numberOfEntities; j++) {
				CollidableEntity target = layer1.get(j);
				// Make sure we are colliding the base with itself
				if (base == target) continue;

				// If the target is not currently active for collisions skip it
				if (!target.getActive()) continue;

				// Check if the collision is allowed to happen
				if (base.shouldCollideWith(target) && target.shouldCollideWith(base)) {

					// Check if the intersect
					if (base.doesIntersect(target.getBoundingBox())) {

						// If the base is a powerup then apply it
						if (base instanceof EntityPowerup) {
							if(((EntityPowerup) base).getCurrentPowerup() instanceof LargerPaddlePowerup) {
								((LargerPaddlePowerup)((EntityPowerup) base).getCurrentPowerup()).setOldPaddle((EntityPaddle) target);
							}
							if(((EntityPowerup) base).getCurrentPowerup() instanceof ExtraBallPowerup) {
								((ExtraBallPowerup)((EntityPowerup) base).getCurrentPowerup()).setHitPaddle((EntityPaddle) target);
							}
							
							if(((EntityPowerup) base).getCurrentPowerup() instanceof AIPowerup) {
								((AIPowerup)((EntityPowerup) base).getCurrentPowerup()).setHitPaddle((EntityPaddle) target);
							}
							
							EventEngine.raisePowerupHitPaddleEvent((EntityPowerup) base, (EntityPaddle) target);
							continue;
						}

						if (base instanceof EntityBall && target instanceof EntityPaddle) {
							EventEngine.raiseBallHitPaddleEvent((EntityPaddle) target, (EntityBall) base);
						}

						// Apply the collision
						doCollision(base, target, delta);
					}
				}
			}
		}
	}

	// Run collision logic for given ICollidable's
	private void doCollision(ICollidable base, ICollidable target, float delta) {
		// Step the base back to that it is no longer in the target
		if(base instanceof ITickable) {
			((ITickable) base).tick(-delta);
		}

		// c = center of the ball
		Vector2f baseCenter = new Vector2f(base.getBoundingBox().getCenter());

		// Initialise loop variables
		Line collisionPlane = getClosestLineToPoint(target.getBoundingBox(), baseCenter);

		// Get the line the ball collided with
		lastCollisionLine = collisionPlane;

		// Initialise the point it collided with
		Vector2f collisionPoint = new Vector2f();
		// Get the collision point
		collisionPlane.getClosestPoint(baseCenter, collisionPoint);

		// Normal between the two entities
		Vector2f normal = baseCenter.copy().sub(collisionPoint).normalise();

		// Tangent between the two entities
		Vector2f tangent = normal.getPerpendicular();

		// Copy into the debug vars
		lastCollisionPoint = collisionPoint.copy();
		lastNormal = normal.copy();
		lastTangent = tangent.copy();

		// Set the point to be reflected against the normal
		Vector2f reflectedPoint = collisionPoint.copy().sub(base.getVelocity());

		// Determine the half length vector to move the point (half is onto the
		// normal)
		Vector2f halfTranslation = tangent.copy().scale((base.getVelocity().dot(tangent)));

		// Reflect the point by translating twice the perpendicular distance to
		// the normal along the tangent plane
		reflectedPoint.add(halfTranslation.scale(2));

		// The new velocity is from the collision point to the new reflected
		// point
		base.setVelocity(reflectedPoint.sub(collisionPoint));

		// If we collided with the brick, tell it to be damaged
		if (target instanceof EntityBrick) {
			EventEngine.raiseBrickDamageEvent((EntityBrick) target, (EntityBall) base);
		}

		if (target instanceof Boundary) {
			// Check if it is the bottom boundary
			if (base instanceof EntityBall) {
				// FIXME: This is a terrible way, fix it
				if (target.getPosition().y >= GameInfo.GAME_HEIGHT) {
					EventEngine.raiseBallHitFloorEvent((EntityBall) base);
				}
			}

			// Destroy powerups when they hit the ground
			if (base instanceof EntityPowerup) {
				((EntityPowerup) base).setDead();
			}
		}
	}

	// Get the line on a shape that is closest to a point
	private Line getClosestLineToPoint(Shape shape, Vector2f point) {
		// Initialise loop variables
		Line closestLine = null;
		float closestDistance = Float.MAX_VALUE;
		int numPoints = shape.getPointCount();

		// Loop through all vertices in the shape to find the closest line,
		// assumes a closed shape
		for (int i = 0; i < numPoints; i++) {
			// Set the index of the next point
			int j = i + 1;

			// Overflow the next point to zero
			if (j >= numPoints) j = 0;

			// Create vectors for the start and end
			Vector2f start = new Vector2f(shape.getPoint(i));
			Vector2f end = new Vector2f(shape.getPoint(j));

			// Construct the line from the points
			Line tempLine = new Line(start, end);

			// If we haven't defined the collision plane yet
			if (closestLine == null) {
				// Cache the distance
				closestDistance = tempLine.distanceSquared(point);
				closestLine = tempLine;
				continue;
			}

			// Check if the line is closer than the previous closest
			float currentDistance = tempLine.distanceSquared(point);
			if (currentDistance < closestDistance) {
				// Cache the distance
				closestDistance = currentDistance;
				closestLine = tempLine;
			}
		}
		return closestLine;
	}

	// Register an entity as physics enabled
	public void registerEntity(Entity e, PhysicsLayer layer) throws BallisticBeesException {
		// Make sure we are putting it in a valis layer
		if (layer != PhysicsLayer.NO_COLLISION && !(e instanceof CollidableEntity)) throw new ObjectNotCollidableException();

		// Make sure we aren't adding it twice
		if (allEntities.contains(e)) throw new AlreadyInWorldException();

		// Add it the list of entities in the world
		allEntities.add(e);

		// Add it to it's particular layer
		switch (layer) {
			case NO_COLLISION:
				layerNoCollision.add(e);
				break;
			case LAYER_1:
				layer1.add((CollidableEntity) e);
				break;
			case LAYER_2:
				layer2.add((CollidableEntity) e);
				break;
			case LAYER_3:
				layer3.add((CollidableEntity) e);
				break;
		}
	}

	// Move and enity to anothe layer
	public void moveEntityToLayer(Entity e, PhysicsLayer layer) throws BallisticBeesException {
		// Make sure the destination layer is valid
		if (layer != PhysicsLayer.NO_COLLISION && e instanceof ICollidable) { throw new ObjectNotCollidableException(); }

		// Remove it from all layers
		// FIXME: This is a crap way of doing it...
		layerNoCollision.remove(e);
		layer1.remove(e);
		layer2.remove(e);
		layer3.remove(e);

		// Add it to the destination layer
		switch (layer) {
			case NO_COLLISION:
				layerNoCollision.add(e);
				break;
			case LAYER_1:
				layer1.add((CollidableEntity) e);
				break;
			case LAYER_2:
				layer2.add((CollidableEntity) e);
				break;
			case LAYER_3:
				layer3.add((CollidableEntity) e);
				break;
		}
	}

	public void clearLayer(PhysicsLayer layer) {
		switch (layer) {
			case LAYER_1:
				for (Entity e : layer1) {
					allEntities.remove(e);
				}
				layer1.clear();
				break;
			case LAYER_2:
				for (Entity e : layer2) {
					allEntities.remove(e);
				}
				layer2.clear();
				break;
			case LAYER_3:
				for (Entity e : layer3) {
					allEntities.remove(e);
				}
				layer3.clear();
				break;
			case NO_COLLISION:
				for (Entity e : layerNoCollision) {
					allEntities.remove(e);
				}
				layerNoCollision.clear();
				break;
		}
	}

	// Remove the entity from the world
	public void removeEntity(Entity e) {
		allEntities.remove(e);
		layerNoCollision.remove(e);
		layer1.remove(e);
		layer2.remove(e);
		layer3.remove(e);
	}

	// Set the boundaries for the world
	// TODO: make boundaries per layer
	public void addGameBounds(int x, int y, int width, int height) {
		boundaries.add(new Boundary(x, y - 20, width, 20)); // Top
		boundaries.add(new Boundary(x - 20, y, 20, height)); // Left
		boundaries.add(new Boundary(width, y, 20, height)); // Right
		boundaries.add(new Boundary(x, height, width, 20)); // Bottom
	}

	// Draw physics debug info, only called if flag is set
	@Override
	public void draw(float x, float y, Graphics g) {

		g.setColor(Color.black);
		for (Boundary boundary : boundaries) {
			g.fillRect(boundary.getPosition().getX(), boundary.getPosition().getY(), boundary.getBoundingBox().getWidth(), boundary.getBoundingBox().getHeight());
		}

		g.setColor(Color.orange);

		if (lastCollisionLine != null) {
			g.draw(lastCollisionLine);
		}

		if (lastNormal != null && lastTangent != null && lastCollisionPoint != null) {
			g.drawGradientLine(lastCollisionPoint.x, lastCollisionPoint.y, Color.red, lastCollisionPoint.x + (lastNormal.x * 20), lastCollisionPoint.y + (lastNormal.y * 20), Color.green);
			g.drawGradientLine(lastCollisionPoint.x, lastCollisionPoint.y, Color.yellow, lastCollisionPoint.x + (lastTangent.x * 20), lastCollisionPoint.y + (lastTangent.y * 20), Color.black);
			g.drawOval(lastCollisionPoint.x, lastCollisionPoint.y, 3, 3);
		}

		for (Entity e : allEntities) {
			if (e.getVelocity().lengthSquared() != 0) {
				g.drawGradientLine(e.getBoundingBox().getCenterX(), e.getBoundingBox().getCenterY(), Color.red, e.getBoundingBox().getCenterX() + e.getVelocity().x / 3f, e.getBoundingBox().getCenterY() + e.getVelocity().y / 3f, Color.green);
			}
		}

		g.setColor(Color.blue);
		g.drawString("Physics TPS: " + lastUps, 50, GameInfo.GAME_HEIGHT - 30);
	}
}
