package com.fraserkillip.ballisticbees.physics;

import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;

import com.fraserkillip.ballisticbees.physics.BodyDef.BodyType;


// Defines a static boundary
public class Boundary implements ICollidable {
	
	// Declare variables
	Vector2f position;
	private Shape boundingBox;
	private static final Vector2f velocity = new Vector2f(0,0); // A boundary never moves
	private static final BodyDef bodyDef = new BodyDef(BodyType.STATIC, -1f, 1f); // A boundary is static can has no mass or restitution
	
	// Constructor
	public Boundary(float x, float y, float width, float height) {
		// Init the bounding box
		boundingBox = new Rectangle(x, y, width, height);
		
		// Init the position
		position = new Vector2f(x, y);
	}

	@Override
	public BodyDef getBodyDef() {
		return bodyDef;
	}

	@Override
	public Shape getBoundingBox() {
		return boundingBox;
	}

	@Override
	public boolean getActive() {
		// The boundary should always be active
		return true;
	}

	@Override
	public void setActive(boolean active) {}

	@Override
	public boolean shouldCollideWith(ICollidable target) {
		// Boundaries collide with all collidable entities
		return true;
	}

	@Override
	public void didCollideWith(ICollidable target) {}

	@Override
	public Vector2f getPosition() {
		return position;
	}

	@Override
	public Vector2f getVelocity() {
		return velocity;
	}

	@Override
	public void setVelocity(Vector2f velocity) {}

}
