package com.fraserkillip.ballisticbees.physics;

/**
 * Contains data for physics collisions
 * 
 * @author fraserkillip
 *
 */
public class BodyDef {

	enum BodyType {
		// Body can only be moved manually, EG infinite mass
		STATIC,
		// Body will move with forces and bounce off objects
		DYNAMIC
	}
	
	// The type of the body
	private BodyType bodyType;
	// This effects the momentum when bouncing off other dynamic bodies
	private float mass;
	// Effects the velocity after bounce
	private float restitution;
	
	public BodyDef(BodyType bodyType, float mass, float restitution) {
		this.bodyType = bodyType;
		this.mass = mass;
		this.restitution = restitution;
	}

	public BodyType getBodyType() {
		return bodyType;
	}

	public void setBodyType(BodyType bodyType) {
		this.bodyType = bodyType;
	}

	public float getMass() {
		return mass;
	}

	public void setMass(float mass) {
		this.mass = mass;
	}

	public float getRestitution() {
		return restitution;
	}

	public void setRestitution(float restitution) {
		this.restitution = restitution;
	}

}
