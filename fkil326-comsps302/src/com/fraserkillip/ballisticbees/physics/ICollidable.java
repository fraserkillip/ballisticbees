package com.fraserkillip.ballisticbees.physics;

import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;

/**
 * ICollidable defines a set of functions that enable the object to interact in the physics engine
 * 
 * @author fraserkillip
 *
 */
public interface ICollidable {

	// The definition of the body
	public BodyDef getBodyDef();
	
	// Get the shapes bounding box
	public Shape getBoundingBox();
	
	// Gets whether the object is active in the physics world
	public boolean getActive();
	
	// Sets the object to be active
	public void setActive(boolean active);
	
	// Checks if a collision with the target is allowed
	public boolean shouldCollideWith(ICollidable target);
	
	// Notifies that the object did collide with the target
	public void didCollideWith(ICollidable target);
	
	// Gets the objects current position
	public Vector2f getPosition();
	
	// Gets the objects velocity
	public Vector2f getVelocity();
	
	// Sets the objects velocity
	public void setVelocity(Vector2f velocity);
}
