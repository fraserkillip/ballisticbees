package com.fraserkillip.ballisticbees.level;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;

import com.fraserkillip.ballisticbees.BallisticBees;
import com.fraserkillip.ballisticbees.common.ITickable;
import com.fraserkillip.ballisticbees.entity.brick.EntityBrick;
import com.fraserkillip.ballisticbees.exceptions.BallisticBeesException;
import com.fraserkillip.ballisticbees.graphics.IRenderable;
import com.fraserkillip.ballisticbees.pack.definitions.LevelDefinition.LevelRowOrder;
import com.fraserkillip.ballisticbees.pack.definitions.LevelDefinition.LevelType;
import com.fraserkillip.ballisticbees.pack.definitions.LevelDefinition.LevelWidth;
import com.fraserkillip.ballisticbees.physics.World;
import com.fraserkillip.ballisticbees.physics.World.PhysicsLayer;

public class Level implements IRenderable, ITickable {
	
	private static final Random random = new Random();

	private LevelType type;

	private LevelWidth width;

	private LevelRowOrder rowOrder;

	private int rowsOnScreen;

	private int rowsToPlay;

	private int totalRows;

	private int rowIndex = 0;

	private String[][] rowDef;

	private List<List<EntityBrick>> rows = new ArrayList<List<EntityBrick>>();

	private World world = null;

	public Level(LevelType type, LevelWidth width, LevelRowOrder rowOrder, int rowsOnScreen, int rowsToPlay, String[][] rowDef) {
		this.type = type;
		this.width = width;
		this.rowOrder = rowOrder;
		this.rowsOnScreen = rowsOnScreen;
		this.rowsToPlay = rowsToPlay;
		this.totalRows = rowsToPlay;
		this.rowDef = rowDef;

		// Reverse the order of the array
		for (int i = 0; i < this.rowDef.length / 2; i++) {
			String[] temp = this.rowDef[i];
			this.rowDef[i] = this.rowDef[this.rowDef.length - i - 1];
			this.rowDef[this.rowDef.length - i - 1] = temp;
		}
	}

	@Override
	public void draw(float x, float y, Graphics g) {
		for (List<EntityBrick> row : getDisplayedRows()) {
			for (EntityBrick brick : row) {
				if (brick != null && !brick.isDead()) {
					brick.draw(g);
				}
			}
		}
	}

	public void setWorld(World world) {
		this.world = world;
	}

	public List<List<EntityBrick>> getAllRows() {
		return rows;
	}

	public List<List<EntityBrick>> getDisplayedRows() {
		return rows.subList(0, Math.min(rowsOnScreen, rows.size()));
	}

	public void initRows() {
		for (int i = 1; i <= rowsOnScreen && (rowsToPlay > 0 || type == LevelType.infinite); i++) {
			rows.add(createNextRow(i));
			rowsToPlay--;
		}
	}

	@Override
	public void tick(float delta) {
		int shifts = 0;
		int numRows = rows.size();
		for (int i = 0; i < numRows; i++) {
			if (rowIsDead(rows.get(i))) {
				if (rows.size() - 1 < rowsOnScreen && (rowsToPlay > 0 || type == LevelType.infinite)) {
					shifts++;
					for (int j = i; j < numRows; j++) {
						for (EntityBrick brick : rows.get(j)) {

							brick.getPosition().add(new Vector2f(0, shifts * (45 + 10)));
						}
					}
					rows.add(createNextRow(rows.size()));
					rowsToPlay--;
				}

			}
		}

		for (Iterator<List<EntityBrick>> iter = rows.iterator(); iter.hasNext();) {
			List<EntityBrick> row = (List<EntityBrick>) iter.next();
			if (rowIsDead(row)) {
				iter.remove();
			}
		}
	}

	private boolean rowIsDead(List<EntityBrick> row) {
		boolean allDead = true;

		for (EntityBrick brick : row) {
			allDead &= brick.isDead();
		}
		return allDead;
	}

	public boolean isComplete() {
		boolean allDead = true;
		for (List<EntityBrick> row : getDisplayedRows()) {
			allDead &= rowIsDead(row);
		}
		return allDead;
	}

	public List<EntityBrick> createNextRow(int rowNum) {
		if (rowOrder == LevelRowOrder.random) {
			rowIndex = random.nextInt(rowDef.length);
		} else if (rowIndex >= rowDef.length) {
			rowIndex = 0;
		}
		List<EntityBrick> brickRow = new ArrayList<EntityBrick>();

		Pattern pipe = Pattern.compile("|", Pattern.LITERAL);

		for (int j = 0; j < rowDef[rowIndex].length; j++) {

			String brickNames = rowDef[rowIndex][j];

			String[] availableBricks = pipe.split(brickNames);

			try {
				EntityBrick brick = BallisticBees.getBrickRegistry().getBrick(availableBricks[random.nextInt(availableBricks.length)]);
				brick.setPosition(new Vector2f(j * (100 + 10) + 20, ((type == LevelType.infinite ? rowsOnScreen : Math.min(rowsOnScreen, totalRows)) - rowNum) * (55) + 20));
				brickRow.add(brick);
				if (world != null) world.registerEntity(brick, PhysicsLayer.LAYER_1);
			} catch (BallisticBeesException e) {
				brickRow.add(null);
				e.printStackTrace();
			}
		}
		rowIndex++;
		return brickRow;
	}

}
