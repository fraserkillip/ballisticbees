package com.fraserkillip.ballisticbees;

/**
 * Constants for the Game for easy reference
 * 
 * 
 * @author fraserkillip
 *
 */
public class GameInfo {
	public static final String GAME_TITLE = "Ballistic Bees";
	public static final int GAME_WIDTH = 800;
	public static final int GAME_HEIGHT = 600;

	public static final String IMAGE_PATH = "assets/images/";
}
